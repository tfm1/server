
FROM openjdk:14-jdk-alpine AS build-image
WORKDIR /application

# Copy only the necessary
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src
# Just download things if it is necessary
RUN ./mvnw dependency:go-offline -B
RUN ./mvnw install -DskipTests

# Create the jar
# hadolint ignore=DL3003
RUN mkdir -p target/dependency && (cd target/dependency || return; jar -xf ../*.jar)



FROM openjdk:14-jdk-alpine

ARG DEPENDENCY=/application/target/dependency

COPY --from=build-image ${DEPENDENCY}/BOOT-INF/lib /stockapplication/lib
COPY --from=build-image ${DEPENDENCY}/META-INF /stockapplication/META-INF
COPY --from=build-image ${DEPENDENCY}/BOOT-INF/classes /stockapplication

#RUN groupadd -r ftmdaniel && useradd -r -s /bin/false -g ftmdaniel userserver
RUN addgroup --system ftmdaniel && adduser --system --disabled-password --no-create-home userserver ftmdaniel
RUN chown -R userserver:ftmdaniel /stockapplication
USER userserver

ENTRYPOINT ["java","-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}","-cp","stockapplication:stockapplication/lib/*","com.university.dferreiropresedo.tfm.TfmApplication"]