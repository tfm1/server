create table if not exists product_type
(
	product_type_id bigint not null
		primary key,
	measure varchar(255) not null,
	name varchar(255) not null
);

create table if not exists product
(
	product_id bigint not null
		primary key,
	name varchar(255) not null,
	tag varchar(255) not null,
	product_type_id bigint not null,
	constraint FK_product_product_type
		foreign key (product_type_id) references product_type (product_type_id)
);

insert ignore into product_type(product_type_id, measure, name) VALUES (1, "L", "Beverage");
insert ignore into product_type(product_type_id, measure, name) VALUES (2, "Kg", "Food");
insert ignore into product_type(product_type_id, measure, name) VALUES (3, "Units", "Misc");

insert ignore into product(product_id, name, tag, product_type_id) VALUES (1, "Water", "AGUA", 1);
insert ignore into product(product_id, name, tag, product_type_id) VALUES (2, "Coke", "COCA", 1);
insert ignore into product(product_id, name, tag, product_type_id) VALUES (3, "Chicken", "POLLO", 2);
insert ignore into product(product_id, name, tag, product_type_id) VALUES (4, "Cow", "TERNERA", 2);
insert ignore into product(product_id, name, tag, product_type_id) VALUES (5, "Trash bags", "BASURA", 3);