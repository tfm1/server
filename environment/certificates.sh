#!/bin/bash

mkdir certificates && cd certificates && mkdir client && mkdir server || exit

# Generate the server keystore and the server certificate
keytool -genkeypair -alias tfm-server -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore ./server/serverKeyStore.p12 -validity 3650 \
  -dname "cn=Server, ou=TFM, o=UDC, L=Coruna, ST=Galicia ,c=ES" -storePass serverKeystorePass

keytool -export -alias tfm-server -storePass serverKeystorePass -file ./server/serverCertificate.cer -keystore ./server/serverKeyStore.p12


# Generate the client keystore and the client certificate
keytool -genkeypair -alias tfm-client -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore ./client/clientKeyStore.p12 -validity 3650 \
  -dname "cn=Client, ou=TFM, o=UDC, L=Coruna, ST=Galicia ,c=ES" -storePass clientKeystorePass

keytool -export -alias tfm-client -storePass clientKeystorePass -file ./client/clientCertificate.cer -keystore ./client/clientKeyStore.p12

# Now we import the client certificate into the server keystore and viceversa
keytool -import -v -trustcacerts -alias tfm-client -file ./client/clientCertificate.cer -keystore ./server/serverKeyStore.p12 \
  -keypass serverKeystorePass -storePass serverKeystorePass -noprompt

keytool -import -v -trustcacerts -alias tfm-server -file ./server/serverCertificate.cer -keystore ./client/clientKeyStore.p12 \
  -keypass clientKeystorePass -storePass clientKeystorePass -noprompt