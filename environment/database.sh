#!/bin/bash

CONTAINER_NAME=ftmdatabase
DB_DATABASE=development_db
DB_USER=developer_user
DB_PASS=developer_pass
DB_ROOT_PASS=rootpass

if [ "$(docker ps -a -f name=$CONTAINER_NAME)" ]
then
  docker stop $CONTAINER_NAME
  docker rm $CONTAINER_NAME
fi

docker pull mysql/mysql-server:8.0

docker run --name $CONTAINER_NAME -it -d -p 3306:3306 \
  -e MYSQL_DATABASE=$DB_DATABASE  -e MYSQL_USER=$DB_USER -e MYSQL_PASSWORD=$DB_PASS -e MYSQL_ROOT_PASSWORD=$DB_ROOT_PASS \
  -v localdata:/var/lib/mysql  -v "$PWD"/populate.sql:/docker-entrypoint-initdb.d/populate.sql -t mysql/mysql-server:8.0