# Grobivo - Servidor

Este proyecto forma parte del Trabajo de Fin de Máster de Daniel Ferreiro Presedo.

## Qué es

Grobivo trata de una aplicación móvil que permite conectarse a un dispositivo Google Home y asociarle un inventario al cual podremos asociar una serie de productos. A mayores, podremos crearles reglas a estos productos para que cuando solicitemos la lista de la compra nos comunique cuáles son los productos que nos faltan en casa. Una vez hayamos hecho la compra, podremos actualizar nuestro inventario a través del escaneo del ticket.

Gracias al servidor de Grobivo, también podremos sincronizarnos con el resto de los usuarios asociados al dispositivo, e incluso gestionarlos siempre que dispongamos de los permisos necesarios.

## Funcionalidades

- Autenticación a través de Google.
- Gestionar los dispositivos y sus correspondientes inventarios.
- Posibilidad de crear reglas sobre los productos del inventario.
- Solicitar la lista de la compra.
- Gestionar los usuarios asociados al dispositivo.
- Escanear los tickets de la compra.

## Instalación

Para arrancar el servidor de Grobivo es necesario tener instalado Docker. Una vez descargado el proyecto, tendremos que configurar el fichero llamada _.env_ con las correspondientes variables:


```txt
DB_NAME= Nombre de la base de datos
DB_USER= Usuario de la base de datos
DB_PASSWORD= Contraseña del usuario
DB_ROOT_PASSWORD= Contraseña del usuario root
DB_PORT= Puerto en el que se abrirá la base de datos
SERVER_HTTPS_PORT= Puerto en el que se levanta el servidor
GOOGLE_AUTH_ID= Clave de autenticacion de Google para configurar la autenticación
GOOGLE_AUTH_SECRET= Secreto de autenticación de Google para configurar la autenticación
IFTTT_KEY= Clave del IFTTT
```

Nos establecemos en la raíz del mismo y ejecutaremos el siguiente comando

```sh
docker-compose up
```

A continuación se nos creará los siguientes componentes Docker, escuchando el servidor en el puerto definido anteriormente:

- Una instancia de la base de datos que se llamará _server\_ftmdatabase\_XXX_.
- Una instancia del servidor que se llamará _server\_ftmserver\_XXX_.
- Un volumen Docker denominado _ftmdata_.
- Una red llamada _ftm-application-net_.

## Licencia

MIT
