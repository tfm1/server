
-- docker run --name appdatabase -p 60600:3306 -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_ROOT_HOST=% -d mysql/mysql-server:8.0


create database app_db;

create user 'appuser'@'%' identified by 'apppassword';

grant DELETE, INDEX, INSERT, LOCK TABLES, SELECT, UPDATE, TRIGGER on app_db.* to 'appuser'@'%';