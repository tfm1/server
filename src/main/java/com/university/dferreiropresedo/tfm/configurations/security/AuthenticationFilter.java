package com.university.dferreiropresedo.tfm.configurations.security;

import com.university.dferreiropresedo.tfm.controller.UserController;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.services.AuthenticationService;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class AuthenticationFilter extends BasicAuthenticationFilter {

    private final AuthenticationService authenticationService;

    public AuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationService authService) {
        super(authenticationManager);
        this.authenticationService = authService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null) {
            chain.doFilter(request, response);
            return;
        }

        boolean isOAuth = authorizationHeader.startsWith(UserController.TOKEN_PREFIX_OAUTH);
        boolean isBasic = authorizationHeader.startsWith(UserController.TOKEN_PREFIX_BASIC);

        if (isOAuth) {
            UsernamePasswordAuthenticationToken authentication = this.validateOAuthHeader(authorizationHeader);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } else if (isBasic) {
            UsernamePasswordAuthenticationToken authentication = this.validateBasicHeader(authorizationHeader);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken validateBasicHeader(String authHeader) {
        String accessToken = authHeader.split(" ")[1];
        User user = this.authenticationService.validateBasicUser(accessToken);
        SimpleGrantedAuthority deviceRole = new SimpleGrantedAuthority(GrantedRole.DEVICE.getRole());

        return new UsernamePasswordAuthenticationToken(user.getEmail(), null, Collections.singletonList(deviceRole));
    }

    private UsernamePasswordAuthenticationToken validateOAuthHeader(String authHeader) {
        String accessToken = authHeader.split(" ")[1];

        User user = this.authenticationService.validateOAuthUser(accessToken);
        if (user == null) {
            return null;
        }

        List<SimpleGrantedAuthority> authorities = this.manageUserAuthorities(user);
        return new UsernamePasswordAuthenticationToken(user.getEmail(), null, authorities);
    }

    private List<SimpleGrantedAuthority> manageUserAuthorities(User user) {

        List<GrantedRole> userRoles = new ArrayList<>();
        userRoles.add(GrantedRole.USER);
        if (user.isAdmin()) {
            userRoles.add(GrantedRole.ADMIN);
        }

        return userRoles.stream()
                .map(GrantedRole::getRole)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }


}
