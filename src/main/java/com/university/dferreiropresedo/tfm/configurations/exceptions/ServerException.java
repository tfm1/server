package com.university.dferreiropresedo.tfm.configurations.exceptions;

public class ServerException extends RuntimeException {

    private final ErrorCode errorCode;
    private final Exception exception;

    public ServerException(ErrorCode errorCode, String message) {
        super(message, new Exception(message));
        this.errorCode = errorCode;
        this.exception = new Exception(message);
    }

    public ServerException(ErrorCode errorCode, Exception exception, String message) {
        super(message, exception.getCause());
        this.errorCode = errorCode;
        this.exception = exception;
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    public Exception getException() {
        return this.exception;
    }

}
