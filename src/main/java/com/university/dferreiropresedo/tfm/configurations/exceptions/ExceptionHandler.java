package com.university.dferreiropresedo.tfm.configurations.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandler.class);

    @org.springframework.web.bind.annotation.ExceptionHandler(value = {RuntimeException.class})
    protected ResponseEntity<Object> handleError(RuntimeException ex, WebRequest request) {

        ExceptionInfo information = new ExceptionInfo(ErrorCode.GENERAL_ERROR, ex.getMessage());
        LOGGER.warn("Uncontrolled exception", ex);
        return this.handleExceptionInternal(ex, information, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


    @org.springframework.web.bind.annotation.ExceptionHandler(value = {ServerException.class})
    protected ResponseEntity<Object> handleError(ServerException ex, WebRequest request) {

        ExceptionInfo information = new ExceptionInfo(ex.getErrorCode(), ex.getMessage());
        LOGGER.warn("There was a controlled error", ex);
        return this.handleExceptionInternal(ex.getException(), information, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

}
