package com.university.dferreiropresedo.tfm.configurations.security;

public enum GrantedRole {

    USER("ROLE_USER"),

    ADMIN("ROLE_ADMIN"),

    DEVICE("ROLE_DEVICE");

    private final String role;

    GrantedRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return this.role;
    }
}
