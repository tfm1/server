package com.university.dferreiropresedo.tfm.configurations.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ErrorCode {

    GENERAL_ERROR(500, "Uncontrolled error"),


    SIGN_IN_FAILED(10000, "Sign in error"),
    USER_NOT_FOUND(10100, "The requested user is not valid."),
    USER_NOT_PRIVILEGED(10200, "The user cannot perform this action."),

    IO_OPERATION_FAILED(14000, "There was a problem reading a file."),
    INVALID_PARAMETER(14001, "Mandatory parameter missing");

    private final int errorCode;
    private final String reason;

    ErrorCode(int errorCode, String reason) {
        this.errorCode = errorCode;
        this.reason = reason;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getReason() {
        return this.reason;
    }
}
