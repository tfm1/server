package com.university.dferreiropresedo.tfm.configurations.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
public class ExceptionInfo {

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    private final ErrorCode error;

    private final String cause;

}
