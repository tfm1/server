package com.university.dferreiropresedo.tfm.services.user;

import com.university.dferreiropresedo.tfm.entities.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService {

    User registerUser(User userInformation);

    User loadUserData(User payloadUser);

    Optional<User> retrieveUserInformation(String userEmail);

    void upgradeUsers(List<User> users, String userEmail);

    List<User> retrievePossibleUsersToAdd(List<String> usersId, String userEmail);

    List<User> retrieveUsersInformation(List<String> usersEmails);
}
