package com.university.dferreiropresedo.tfm.services.rule;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.repositories.InventoryRepository;
import com.university.dferreiropresedo.tfm.repositories.RuleRepository;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RuleServiceImpl implements RuleService {


    @Autowired
    private UserService userService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    @Transactional
    public List<Rule> createRules(List<Rule> rules, String userEmail) {
        List<Rule> rulesCreated = new ArrayList<>();
        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optUserFound.isPresent()) {
            User userFound = optUserFound.get();
            if (userFound.isAdmin()) {
                for (Rule rule : rules) {
                    Optional<Inventory> optInventoryFound = this.inventoryRepository.findById(rule.getInventory().getId());
                    if (optInventoryFound.isPresent()) {
                        Inventory inventory = optInventoryFound.get();
                        rule.setInventory(inventory);
                        Rule ruleSaved = this.ruleRepository.save(rule);
                        rulesCreated.add(ruleSaved);
                        inventory.addRule(ruleSaved);
                        this.inventoryRepository.save(inventory);
                    }
                }
            } else {
                throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this action");
            }
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user does not exist");
        }

        return rulesCreated;
    }

    @Override
    @Transactional
    public void removeRules(List<Long> rules, String userEmail) {
        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optUserFound.isPresent()) {
            User userFound = optUserFound.get();
            if (userFound.isAdmin()) {

                for (Rule rule : this.ruleRepository.findAllById(rules)) {
                    rule.removeRuleFromInventory();
                    this.ruleRepository.delete(rule);
                    this.inventoryRepository.save(rule.getInventory());
                }

            } else {
                throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this action");
            }
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user does not exist");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rule> retrieveInventoryRules(Long inventoryId, String userEmail) {
        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optUserFound.isPresent()) {
            Optional<Inventory> optInventoryFound = this.inventoryRepository.findById(inventoryId);

            if (!optInventoryFound.isPresent()) {
                throw new ServerException(ErrorCode.INVALID_PARAMETER, "The inventory was not found");
            } else {
                Inventory inventoryFound = optInventoryFound.get();
                inventoryFound.getRules().size();
                return inventoryFound.getRules();
            }
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    @Transactional
    public void updateRules(Device device, List<Rule> rules, String userEmail) {
        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optUserFound.isPresent()) {
            Inventory inventoryFound = this.deviceService.retrieveDeviceInventory(device, userEmail);
            List<Rule> rulesFound = inventoryFound.getRules();
            List<Rule> rulesToInsert = new ArrayList<>();
            List<Rule> rulesToUpdate = new ArrayList<>();
            for (Rule ruleToUpdate : rules) {
                Optional<Rule> optRuleFound = rulesFound.stream().filter(r -> r.getProduct().getId() == ruleToUpdate.getProduct().getId()).findFirst();
                if (optRuleFound.isPresent()) {
                    Rule ruleFound = optRuleFound.get();
                    ruleFound.setAmount(ruleToUpdate.getAmount());
                    rulesToUpdate.add(ruleFound);
                } else {
                    rulesToInsert.add(ruleToUpdate);
                    inventoryFound.addRule(ruleToUpdate);
                }
            }

            this.ruleRepository.saveAll(rulesToUpdate);
            this.ruleRepository.saveAll(rulesToInsert);
            this.inventoryRepository.save(inventoryFound);
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user was not found");
        }
    }
}
