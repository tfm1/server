package com.university.dferreiropresedo.tfm.services.user;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.configurations.security.GrantedRole;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public User registerUser(User userInformation) {
        User userFound = this.userRepository.findByInternalId(userInformation.getInternalId());
        if (userFound == null) {
            userFound = this.userRepository.save(userInformation);
        }

        return userFound;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> retrieveUserInformation(String userEmail) {
        return Optional.ofNullable(this.userRepository.findByEmail(userEmail));
    }

    @Override
    public void upgradeUsers(List<User> users, String userEmail) {
        User userFound = this.userRepository.findByEmail(userEmail);
        if (userFound.isAdmin()) {
            List<String> usersEmail = users.stream().map(User::getEmail).collect(Collectors.toList());
            List<User> usersFound = this.userRepository.findByEmailIn(usersEmail);

            for (User user : usersFound) {
                user.setAdmin(true);
            }
            this.userRepository.saveAll(usersFound);
        } else {
            throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this operation");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> retrievePossibleUsersToAdd(List<String> usersId, String userEmail) {
        User userFound = this.userRepository.findByEmail(userEmail);
        if (userFound.isAdmin()) {
            Iterable<User> all = this.userRepository.findByInternalIdNotIn(usersId);
            List<User> usersFound = new ArrayList<>();
            for (User user : all) {
                usersFound.add(user);
            }
            return usersFound;
        } else {
            throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this operation");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> retrieveUsersInformation(List<String> usersEmails) {
        return this.userRepository.findByEmailIn(usersEmails);
    }

    @Override
    @Transactional(readOnly = true)
    public User loadUserData(User payloadUser) {
        return this.userRepository.findByInternalId(payloadUser.getInternalId());
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User userFound = this.userRepository.findByEmail(s);
        List<SimpleGrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority(GrantedRole.DEVICE.getRole()));
        if (userFound == null) {
            throw new UsernameNotFoundException("The user was not found");
        }
        return new org.springframework.security.core.userdetails.User(s, userFound.getInternalId(), authorities);
    }
}
