package com.university.dferreiropresedo.tfm.services.device;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.repositories.DeviceRepository;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private UserService userService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private DeviceRepository deviceRepository;


    @Override
    public List<Device> getUserDevices(String userEmail) {

        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optUserFound.isPresent()) {
            User userFound = optUserFound.get();
            return this.deviceRepository.findDevicesByUsers_internalIdLike(userFound.getInternalId());
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The requested user was not found");
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Inventory retrieveDeviceInventory(Device device, String userEmail) {
        Optional<User> optionalUserFound = this.userService.retrieveUserInformation(userEmail);
        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(device.getUUID());
        if (optionalUserFound.isPresent() && optionalDeviceFound.isPresent()) {
            return this.inventoryService.retrieveInventoryData(optionalDeviceFound.get());
        }
        return null;
    }

    @Override
    @Transactional
    public Inventory connectUserDevice(Device deviceToConnect, String userEmail) {
        Optional<User> optionalUserFound = this.userService.retrieveUserInformation(userEmail);
        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(deviceToConnect.getUUID());
        Device deviceFound = optionalDeviceFound.orElseGet(() -> new Device(deviceToConnect.getUUID(), deviceToConnect.getName()));
        if (!optionalUserFound.isPresent()) {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user email does not exist.");
        }
        User userFound = optionalUserFound.get();

        deviceFound.addUser(userFound);
        Device deviceSaved = this.deviceRepository.save(deviceFound);

        Inventory inventory = this.inventoryService.retrieveInventoryData(deviceSaved);
        if (inventory == null) {
            inventory = this.inventoryService.associateEmptyInventoryToDevice(deviceSaved);
        }
        int size = inventory.getRules().size();
        int size1 = inventory.getRegisters().size();

        return inventory;
    }


    @Override
    @Transactional
    public void connectUsersToDevice(String deviceUUID, List<String> usersToAdd, String userEmail) {
        Optional<User> optUserFound = this.userService.retrieveUserInformation(userEmail);
        Optional<Device> optDeviceFound = this.deviceRepository.findById(deviceUUID);
        if (!optUserFound.isPresent() || !optDeviceFound.isPresent()) {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user email does not exist");
        }

        User userFound = optUserFound.get();
        Device deviceFound = optDeviceFound.get();
        if (!userFound.isAdmin()) {
            throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this action");
        }

        List<User> usersFound = this.userService.retrieveUsersInformation(usersToAdd);
        for (User user : usersFound) {
            deviceFound.addUser(user);
        }

        this.deviceRepository.save(deviceFound);
    }

    @Override
    @Transactional
    public boolean disconnectUserDevices(List<Device> devicesToDisconnect, String userEmail) {
        boolean removed = false;
        Optional<User> optionalUserFound = this.userService.retrieveUserInformation(userEmail);
        if (optionalUserFound.isPresent()) {
            User userFound = optionalUserFound.get();
            Set<String> uuidsToDisconnect = devicesToDisconnect.stream().map(Device::getUUID).collect(Collectors.toSet());
            List<Device> devicesToRemoveRelation = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(userFound.getInternalId(),
                    new ArrayList<>(uuidsToDisconnect));
            if (!devicesToRemoveRelation.isEmpty()) {
                devicesToRemoveRelation.forEach(device -> device.removeUser(userFound));
                this.deviceRepository.saveAll(devicesToRemoveRelation);
                removed = true;
            }
        }
        return removed;
    }

    @Override
    @Transactional(readOnly = true)
    public Set<User> getAllDeviceUsers(String deviceUUID, String userEmail) {
        Optional<User> optionalUserFound = this.userService.retrieveUserInformation(userEmail);
        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(deviceUUID);

        if (optionalDeviceFound.isPresent() && optionalUserFound.isPresent()) {
            User userFound = optionalUserFound.get();
            Device deviceFound = optionalDeviceFound.get();

            if (userFound.isAdmin()) {
                deviceFound.getUsers().size();
                return deviceFound.getUsers();
            } else {
                throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this action.");
            }
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user email does not exist.");
        }
    }

    @Override
    @Transactional
    public void removeUsersFromDevice(String deviceUUID, List<User> usersToRemove, String userEmail) {
        Optional<User> optUserPerformingAction = this.userService.retrieveUserInformation(userEmail);
        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(deviceUUID);

        if (optionalDeviceFound.isPresent() && optUserPerformingAction.isPresent()) {
            User userPerformingAction = optUserPerformingAction.get();
            Device deviceFound = optionalDeviceFound.get();
            List<String> usersIdToRemove = usersToRemove.stream().map(User::getInternalId).collect(Collectors.toList());

            if (userPerformingAction.isAdmin()) {
                Set<User> users = new HashSet<>(deviceFound.getUsers());
                for (User user : users) {
                    if (usersIdToRemove.contains(user.getInternalId())) {
                        deviceFound.removeUser(user);
                    }
                }
                this.deviceRepository.save(deviceFound);
            } else {
                throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "The user cannot perform this action.");
            }
        } else {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user email does not exist.");
        }
    }

}
