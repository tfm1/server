package com.university.dferreiropresedo.tfm.services.groceries;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroceriesServiceImpl implements GroceriesService{

    @Autowired
    private UserService userService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private DeviceService deviceService;

    @Override
    @Transactional
    public List<Register> requestGroceriesList(Device device, String userEmail) {
        Inventory inventoryFound = deviceService.retrieveDeviceInventory(device, userEmail);
        return calculateGroceriesList(inventoryFound,inventoryFound.getRules(), inventoryFound.getRegisters());
    }

    private List<Register> calculateGroceriesList(Inventory inventory,List<Rule> rulesToApply, List<Register> productsRegistered) {

        List<Register> groceries = new ArrayList<>();

        for (Rule rule : rulesToApply) {
            Optional<Register> optProductToBeAdded = productsRegistered.stream()
                    .filter(register ->
                            register.getProduct().getId() == rule.getProduct().getId() &&
                            register.getAmount() <= rule.getAmount()
                    ).findAny();
            optProductToBeAdded.ifPresent(groceries::add);
        }

        return groceries;
    }
}
