package com.university.dferreiropresedo.tfm.services.product;

import com.university.dferreiropresedo.tfm.entities.Product;

public interface ProductService {

    Product findProductByTag(String productTag);
}
