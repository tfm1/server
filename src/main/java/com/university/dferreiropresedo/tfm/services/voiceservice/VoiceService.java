package com.university.dferreiropresedo.tfm.services.voiceservice;

public interface VoiceService {

    void addProduct(String productTag, Double amount, String userEmail);

    void withdrawProduct(String productTag, Double amount, String userEmail);

}
