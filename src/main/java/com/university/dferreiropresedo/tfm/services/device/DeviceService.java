package com.university.dferreiropresedo.tfm.services.device;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;

import java.util.List;
import java.util.Set;

public interface DeviceService {

    List<Device> getUserDevices(String userEmail);

    Inventory retrieveDeviceInventory(Device device, String userEmail);

    Inventory connectUserDevice(Device deviceToConnect, String userEmail);

    boolean disconnectUserDevices(List<Device> devicesToDisconnect, String userEmail);

    Set<User> getAllDeviceUsers(String deviceUUID, String userEmail);

    void removeUsersFromDevice(String deviceUUID, List<User> usersToRemove, String userEmail);

    void connectUsersToDevice(String deviceUUID, List<String> usersToAdd, String userEmail);
}
