package com.university.dferreiropresedo.tfm.services.rule;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;

import java.util.List;

public interface RuleService {

    List<Rule> createRules(List<Rule> rules, String userEmail);

    void removeRules(List<Long> rules, String userEmail);

    List<Rule> retrieveInventoryRules(Long inventoryId, String userEmail);

    void updateRules(Device device, List<Rule> rules, String userEmail);
}
