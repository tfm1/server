package com.university.dferreiropresedo.tfm.services.groceries;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;

import java.util.List;

public interface GroceriesService {

    List<Register> requestGroceriesList(Device device, String userEmail);

}
