package com.university.dferreiropresedo.tfm.services.inventory;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.repositories.InventoryRepository;
import com.university.dferreiropresedo.tfm.repositories.RegisterRepository;
import lombok.NonNull;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private RegisterRepository registerRepository;


    @Override
    @Transactional(readOnly = true)
    public List<Inventory> retrieveDevicesInventories(List<Device> devices) {
        List<String> deviceUUIDs = devices.stream().map(Device::getUUID).collect(Collectors.toList());
        return this.inventoryRepository.findAllByDeviceUUIDIn(deviceUUIDs);
    }

    @Override
    @Transactional(readOnly = true)
    public Inventory retrieveInventoryData(Device device) {
        return this.inventoryRepository.findByDeviceUUID(device.getUUID());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Register> retrieveDeviceRegisters(String deviceUUID) {
        Inventory inventoryFound = this.inventoryRepository.findByDeviceUUID(deviceUUID);
        inventoryFound.getRegisters().size();
        return inventoryFound.getRegisters();
    }

    @Override
    @Transactional
    public Inventory associateEmptyInventoryToDevice(Device device) {
        Inventory emptyInventory = new Inventory(device);
        return this.inventoryRepository.save(emptyInventory);
    }

    @Override
    @Transactional
    public @NonNull List<Register> updateRegistersInfo(List<Register> newData, User user) {

        if (newData == null || newData.isEmpty()) {
            return Collections.emptyList();
        }

        Long inventoryId = newData.stream().findAny().map(register -> register.getInventory().getId()).get();
        Optional<Inventory> optInventoryFound = this.inventoryRepository.findById(inventoryId);
        if (!optInventoryFound.isPresent()) {
            throw new ServerException(ErrorCode.GENERAL_ERROR, "It was not found any inventory matching the requirements");
        }

        Inventory inventoryFound = optInventoryFound.get();
        if (!inventoryFound.getDevice().getUsers().contains(user)) {
            throw new ServerException(ErrorCode.USER_NOT_PRIVILEGED, "This user cannot perform this action");
        }

        // we search if there already are registers for the incoming data
        List<Long> productIds = newData.stream().map(r -> r.getProduct().getId()).collect(Collectors.toList());
        List<Register> registersFound = this.registerRepository.findRegistersByInventory_idAndProduct_idIn(inventoryId, productIds);
        List<Register> registersToBeDeleted = new ArrayList<>();
        List<Register> registersToBeUpdated = new ArrayList<>();


        for (Register registerFound : registersFound) {
            Optional<Register> optRegisterData = newData.stream().filter(r -> r.getProduct().getId() == registerFound.getProduct().getId()).findAny();
            if (optRegisterData.isPresent()) {
                Register registerData = optRegisterData.get();
                if (registerData.getAmount() == 0D) {
                    // if the amount is set to 0, it is meant to be deleted
                    registersToBeDeleted.add(registerFound);
                    inventoryFound.getRegisters().remove(registerFound);
                } else {
                    // otherwise, it is meant to be updated
                    registerFound.setAmount(registerData.getAmount());
                    registersToBeUpdated.add(registerFound);
                }
            }
        }

        List<Long> productIdsToBeDeleted = registersToBeDeleted.stream().map(r -> r.getProduct().getId()).collect(Collectors.toList());
        List<Long> productIdsToBeUpdated = registersToBeUpdated.stream().map(r -> r.getProduct().getId()).collect(Collectors.toList());

        // calculate the remaining registers to be added
        List<Register> registersToBeAdded = newData.stream()
                .filter(r -> r.getAmount() != 0D &&
                        !productIdsToBeDeleted.contains(r.getProduct().getId()) && !productIdsToBeUpdated.contains(r.getProduct().getId()))
                .peek(r -> r.setId(0L))
                .collect(Collectors.toList());

        this.registerRepository.deleteAll(registersToBeDeleted);
        this.registerRepository.saveAll(registersToBeUpdated);
        this.registerRepository.saveAll(registersToBeAdded);

        inventoryFound.getRegisters().addAll(registersToBeAdded);

        inventoryRepository.save(inventoryFound);
        Hibernate.initialize(inventoryFound.getRegisters());
        return inventoryFound.getRegisters();
    }
}
