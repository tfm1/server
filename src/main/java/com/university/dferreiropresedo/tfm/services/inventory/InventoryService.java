package com.university.dferreiropresedo.tfm.services.inventory;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import lombok.NonNull;

import java.util.List;

public interface InventoryService {

    List<Inventory> retrieveDevicesInventories(List<Device> devices);

    Inventory retrieveInventoryData(Device device);

    List<Register> retrieveDeviceRegisters(String deviceUUID);

    Inventory associateEmptyInventoryToDevice(Device device);

    @NonNull List<Register> updateRegistersInfo(List<Register> registersToUpdate, User user);
}
;