package com.university.dferreiropresedo.tfm.services.voiceservice;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.repositories.InventoryRepository;
import com.university.dferreiropresedo.tfm.repositories.RegisterRepository;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.product.ProductService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class VoiceServiceImpl implements VoiceService {

    @Autowired
    private UserService userService;

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    @Transactional
    public void addProduct(String productTag, Double amount, String userEmail) {

        Inventory inventoryAssociated = getInventoryFromUser(userEmail);
        if (inventoryAssociated == null) {
            return;
        }

        Product productToOperate = productService.findProductByTag(productTag);
        Optional<Register> registerFound = inventoryAssociated.getRegisters().stream()
                .filter(r -> r.getProduct().getId() == productToOperate.getId()).findAny();

        if (registerFound.isPresent()) {
            Register registerToUpdate = registerFound.get();
            registerToUpdate.setAmount(registerToUpdate.getAmount() + amount);
            registerRepository.save(registerToUpdate);

        } else {
            Register registerToCreate = new Register(inventoryAssociated, productToOperate, amount);
            inventoryAssociated.addRegister(registerToCreate);
            this.registerRepository.save(registerToCreate);
            this.inventoryRepository.save(inventoryAssociated);
        }
    }

    @Override
    @Transactional
    public void withdrawProduct(String productTag, Double amount, String userEmail) {
        Inventory inventoryAssociated = getInventoryFromUser(userEmail);
        if (inventoryAssociated == null) {
            return;
        }

        Product productToOperate = productService.findProductByTag(productTag);
        Optional<Register> registerFound = inventoryAssociated.getRegisters().stream()
                .filter(r -> r.getProduct().getId() == productToOperate.getId()).findAny();

        if (registerFound.isPresent()) {
            Register registerToUpdate = registerFound.get();
            double newAmount = registerToUpdate.getAmount() - amount;
            if (newAmount == 0D) {
                inventoryAssociated.getRegisters().remove(registerToUpdate);
                registerRepository.delete(registerToUpdate);
                inventoryRepository.save(inventoryAssociated);
            } else {
                registerToUpdate.setAmount(newAmount);
                registerRepository.save(registerToUpdate);
            }
        }
    }

    private Inventory getInventoryFromUser(String userEmail) {
        Optional<User> userFoundOpt = userService.retrieveUserInformation(userEmail);

        if (userFoundOpt.isPresent()) {
            User userFound = userFoundOpt.get();
            Optional<Device> deviceFoundOpt = userFound.getUsingDevices().stream().findFirst();

            if (deviceFoundOpt.isPresent()) {
                Device deviceFound = deviceFoundOpt.get();
                return this.inventoryService.retrieveInventoryData(deviceFound);
            }
        }
        return null;
    }
}
