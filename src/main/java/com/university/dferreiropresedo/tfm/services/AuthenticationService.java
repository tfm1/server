package com.university.dferreiropresedo.tfm.services;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Collections;
import java.util.Optional;

@Service
public class AuthenticationService {

    private final static Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);

    @Value("${spring.security.oauth2.client.registration.google.client-id}")
    private String clientId;

    @Value("${ifttt.key}")
    private String iftttKey;

    @Autowired
    private UserService userService;

    public User retrieveUserInformationFromAccessToken(String accessToken) {

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
                .setAudience(Collections.singletonList(this.clientId))
                .build();

        try {
            GoogleIdToken idToken = verifier.verify(accessToken);
            if (idToken != null) {
                GoogleIdToken.Payload payload = idToken.getPayload();

                return this.createUserFromPayload(payload);
            }

        } catch (Exception e) {
            LOGGER.warn("There " +
                    "was an exception trying to validate the token", e);
            throw new ServerException(ErrorCode.SIGN_IN_FAILED, e, "The token is not valid");
        }

        return null;
    }


    public User validateOAuthUser(String accessToken) {

        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
                .setAudience(Collections.singletonList(this.clientId))
                .build();

        try {
            GoogleIdToken idToken = verifier.verify(accessToken);
            if (idToken != null) {
                GoogleIdToken.Payload payload = idToken.getPayload();

                User payloadUser = this.createUserFromPayload(payload);
                return this.userService.loadUserData(payloadUser);
            } else {
                throw new ServerException(ErrorCode.SIGN_IN_FAILED, "The token is not valid");
            }

        } catch (Exception e) {
            LOGGER.warn("There was an exception trying to validate the token", e);
            throw new ServerException(ErrorCode.SIGN_IN_FAILED, e, "The token is not valid");
        }
    }

    public User validateBasicUser(String basicToken) {

        String userPassword = new String(Base64.getDecoder().decode(basicToken));
        String[] userPasswordSplits = userPassword.split(":");

        if (!userPasswordSplits[1].equals(iftttKey)) {
            throw new ServerException(ErrorCode.SIGN_IN_FAILED, "The token is not valid");
        }

        Optional<User> userFound = this.userService.retrieveUserInformation(userPasswordSplits[0]);
        if (userFound.isPresent()) {
            return userFound.get();
        } else {
            throw new ServerException(ErrorCode.SIGN_IN_FAILED, "The token is not valid");
        }
    }

    private User createUserFromPayload(GoogleIdToken.Payload payload) {

        String internalId = payload.getSubject();
        String userEmail = payload.getEmail();
        String username = (String) payload.get("name");
        String imageUrl = (String) payload.get("picture");

        User user = new User(internalId, username, userEmail);
        user.setImageUrl(imageUrl);

        return user;
    }

}
