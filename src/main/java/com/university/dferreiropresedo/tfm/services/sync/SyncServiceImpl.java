package com.university.dferreiropresedo.tfm.services.sync;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.DeviceDTO;
import com.university.dferreiropresedo.tfm.dto.DeviceInformationDTO;
import com.university.dferreiropresedo.tfm.dto.InventoryDTO;
import com.university.dferreiropresedo.tfm.dto.UserDeviceInformationDTO;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SyncServiceImpl implements SyncService {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;

    @Autowired
    private InventoryService inventoryService;

    @Override
    @Transactional
    public List<UserDeviceInformationDTO> syncDeviceInformation(String userEmail) {

        List<UserDeviceInformationDTO> result = new ArrayList<>();
        Optional<User> userFoundOpt = this.userService.retrieveUserInformation(userEmail);
        if (!userFoundOpt.isPresent()) {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user was not found");
        }

        List<Device> userDevices = this.deviceService.getUserDevices(userEmail);
        List<Inventory> inventories = this.inventoryService.retrieveDevicesInventories(userDevices);

        for (Device userDevice : userDevices) {
            Optional<Inventory> optInventoryFound = inventories.stream().filter(i -> i.getDevice().getUUID().equals(userDevice.getUUID())).findAny();
            if (optInventoryFound.isPresent()) {
                DeviceInformationDTO deviceDTO = new DeviceInformationDTO(
                        DeviceDTO.fromDevice(userDevice),
                        InventoryDTO.fromInventory(optInventoryFound.get())
                );
                UserDeviceInformationDTO userDeviceInfo = new UserDeviceInformationDTO(userFoundOpt.get().isAdmin(), deviceDTO);
                result.add(userDeviceInfo);
            }
        }

        return result;
    }
}
