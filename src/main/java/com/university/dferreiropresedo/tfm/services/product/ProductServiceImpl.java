package com.university.dferreiropresedo.tfm.services.product;

import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductRepository productRepository;

    @Override
    @Transactional(readOnly = true)
    public Product findProductByTag(String productTag) {
        return productRepository.findByTag(productTag);
    }
}
