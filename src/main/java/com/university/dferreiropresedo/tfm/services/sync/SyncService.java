package com.university.dferreiropresedo.tfm.services.sync;

import com.university.dferreiropresedo.tfm.dto.UserDeviceInformationDTO;

import java.util.List;

public interface SyncService {

    List<UserDeviceInformationDTO> syncDeviceInformation(String userEmail);

}
