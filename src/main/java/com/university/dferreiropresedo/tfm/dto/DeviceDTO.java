package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.Device;
import lombok.Data;

import java.io.Serializable;

@Data
public class DeviceDTO implements Serializable {

    @JsonProperty("uuid")
    private final String UUID;

    @JsonProperty("name")
    private final String name;

    public static DeviceDTO fromDevice(Device device) {
        return new DeviceDTO(device.getUUID(), device.getName());
    }

    public Device toDevice() {
        return new Device(this.UUID, this.name);
    }

}
