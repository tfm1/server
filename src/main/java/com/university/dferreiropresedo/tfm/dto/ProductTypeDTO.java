package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.ProductType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProductTypeDTO {

    @JsonProperty("product_type_id")
    long productTypeId;
    @JsonProperty("name")
    String name;
    @JsonProperty("measure")
    String measure;

    public static ProductTypeDTO fromEntity(ProductType productType) {
        return new ProductTypeDTO(productType.getId(), productType.getName(), productType.getMeasure());
    }

    public ProductType toProductType() {
        ProductType productType = new ProductType(this.name, this.measure);
        productType.setId(this.productTypeId);
        return productType;
    }
}
