package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class InventoryDTO implements Serializable {

    @JsonProperty("device_UUID")
    String deviceUUID;
    @JsonProperty("server_inventory_id")
    Long serverInventoryId;
    @JsonProperty("client_inventory_id")
    Long clientInventoryId;
    @JsonProperty("registers")
    List<RegisterDTO> registers;
    @JsonProperty("rules")
    List<RuleDTO> rules;

    public static InventoryDTO fromInventory(Inventory inventory) {
        return new InventoryDTO(inventory.getDevice().getUUID(), inventory.getId(), 0L, RegisterDTO.fromEntity(inventory.getRegisters()), RuleDTO.fromEntity(inventory.getRules()));
    }

}
