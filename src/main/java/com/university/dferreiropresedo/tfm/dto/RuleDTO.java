package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.stream.Collectors;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class RuleDTO {

    @JsonProperty("client_rule_id")
    Long clientRuleId;
    @JsonProperty("server_rule_id")
    Long serverRuleId;
    @JsonProperty("quantity")
    Float quantity;
    @JsonProperty("inventory_id")
    Long inventoryId;
    @JsonProperty("product")
    ProductDTO product;

    public static RuleDTO fromEntity(Rule rule) {
        return new RuleDTO(0L, rule.getId(), (float) rule.getAmount(), rule.getInventory().getId(), ProductDTO.fromEntity(rule.getProduct()));
    }

    public static List<RuleDTO> fromEntity(List<Rule> rule) {
        return rule.stream().map(RuleDTO::fromEntity).collect(Collectors.toList());
    }

    public Rule toRule(Device device) {
        Inventory inventory = new Inventory(device);
        inventory.setId(this.inventoryId);
        Rule rule = new Rule(inventory, this.product.toProduct(), this.quantity);
        rule.setId(this.serverRuleId == null ? 0L : this.serverRuleId);
        return rule;
    }

}
