package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DeviceRulesDTO {

    @JsonProperty("device")
    private DeviceDTO deviceDTO;

    @JsonProperty("rules")
    private List<RuleDTO> rules;

}