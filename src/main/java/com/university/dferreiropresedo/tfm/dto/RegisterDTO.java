package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;
import java.util.stream.Collectors;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class RegisterDTO {

    @JsonProperty("client_register_id")
    long clientRegisterId;
    @JsonProperty("server_register_id")
    long serverRegisterId;
    @JsonProperty("inventory_id")
    long inventoryId;
    @JsonProperty("quantity")
    double quantity;
    @JsonProperty("product")
    ProductDTO productDTO;

    public static RegisterDTO fromEntity(Register register) {
        return new RegisterDTO(0L, register.getId(), register.getInventory().getId(), register.getAmount(), ProductDTO.fromEntity(register.getProduct()));
    }

    public static List<RegisterDTO> fromEntity(List<Register> register) {
        return register.stream().map(RegisterDTO::fromEntity).collect(Collectors.toList());
    }

    public Register toRegister() {
        Inventory inventory = new Inventory();
        inventory.setId(this.inventoryId);
        Register register = new Register(inventory, this.productDTO.toProduct(), this.quantity);
        register.setId(this.serverRegisterId);
        return register;
    }

}
