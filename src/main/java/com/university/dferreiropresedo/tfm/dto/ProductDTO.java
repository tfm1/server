package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.Product;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ProductDTO {

    @JsonProperty("product_id")
    long productId;
    @JsonProperty("name")
    String name;
    @JsonProperty("productType")
    ProductTypeDTO productTypeDTO;

    public static ProductDTO fromEntity(Product product) {
        return new ProductDTO(product.getId(), product.getName(), ProductTypeDTO.fromEntity(product.getProductType()));
    }

    public Product toProduct() {
        Product product = new Product(this.name, "", this.productTypeDTO.toProductType());
        product.setId(this.productId);
        return product;
    }

}
