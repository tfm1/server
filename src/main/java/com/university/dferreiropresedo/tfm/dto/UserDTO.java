package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.university.dferreiropresedo.tfm.entities.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserDTO implements Serializable {

    @JsonProperty("user_id")
    private final String id;
    @JsonProperty("name")
    private final String name;
    @JsonProperty("email")
    private final String email;
    @JsonProperty("admin")
    private final boolean isAdmin;
    @JsonProperty("image_url")
    private final String imageUrl;

    public static UserDTO fromUser(User userEntity) {
        return new UserDTO(userEntity.getInternalId(), userEntity.getName(), userEntity.getEmail(), userEntity.isAdmin(), userEntity.getImageUrl());
    }

    public User toUser() {
        return new User(this.id, this.name, this.email);
    }
}


