package com.university.dferreiropresedo.tfm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserDeviceInformationDTO implements Serializable {
    @JsonProperty("isAdmin")
    boolean isAdmin;
    @JsonProperty("userInfo")
    DeviceInformationDTO deviceInformation;
}
