package com.university.dferreiropresedo.tfm.entities;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "product_type")
@SequenceGenerator(name = "product_type_sequence", initialValue = 10)
public class ProductType {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_type_sequence")
    @Column(name = "product_type_id")
    private long id;

    @Column(nullable = false)
    @NonNull
    private String name;

    @Column(nullable = false)
    @NonNull
    private String measure;

    @OneToMany(mappedBy = "productType", cascade = CascadeType.ALL)
    @NonNull
    @ToString.Exclude
    private List<Product> products = new ArrayList<>();


    public List<Product> addProduct(Product product) {
        this.products.add(product);
        return this.products;
    }
}
