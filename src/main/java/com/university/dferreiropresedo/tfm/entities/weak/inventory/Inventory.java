package com.university.dferreiropresedo.tfm.entities.weak.inventory;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "inventory",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"inventory_id", "device_UUID"})
        }
)
@SequenceGenerator(name = "inventory_sequence", initialValue = 10)
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "inventory_sequence")
    @Column(name = "inventory_id", nullable = false)
    private long id;

    @OneToOne
    @NonNull
    @JoinColumn(name = "device_UUID", referencedColumnName = "UUID")
    private Device device;

    @OneToMany(mappedBy = "inventory", cascade = CascadeType.ALL)
    @NonNull
    @ToString.Exclude
    private List<Register> registers = new ArrayList<>();

    @OneToMany(mappedBy = "inventory", cascade = CascadeType.ALL)
    @NonNull
    @ToString.Exclude
    private List<Rule> rules = new ArrayList<>();

    public List<Register> addRegister(Register register) {
        this.registers.add(register);
        return this.registers;
    }

    public List<Rule> addRule(Rule rule) {
        this.rules.add(rule);
        return this.rules;
    }

    public List<Rule> removeRule(Rule rule) {
        this.rules.remove(rule);
        return this.rules;
    }

    public List<Rule> removeRules(List<Rule> rules) {
        this.rules.removeAll(rules);
        return this.rules;
    }
}