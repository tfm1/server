package com.university.dferreiropresedo.tfm.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "product")
@SequenceGenerator(name = "product_sequence", initialValue = 10)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence")
    @Column(name = "product_id", nullable = false)
    private long id;

    @Column(nullable = false)
    @NonNull
    private String name;

    @Column(nullable = false)
    @NonNull
    private String tag;

    @ManyToOne
    @JoinColumn(name = "product_type_id", nullable = false)
    @NonNull
    private ProductType productType;
}
