package com.university.dferreiropresedo.tfm.entities.weak.register;

import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "register",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"register_id", "inventory_id", "product_id"})
        }
)
@SequenceGenerator(name = "register_sequence", initialValue = 10)
public class Register {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "register_sequence")
    @Column(name = "register_id", nullable = false)
    private long id;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "inventory_id", nullable = false)
    private Inventory inventory;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @NonNull
    @Column(nullable = false)
    private double amount;
}
