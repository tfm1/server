package com.university.dferreiropresedo.tfm.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "device")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Device implements Serializable {

    @Id
    @Column(length = 36, name = "UUID")
    @NonNull
    @EqualsAndHashCode.Include
    private String UUID;

    @Column(nullable = false)
    @NonNull
    @EqualsAndHashCode.Include
    private String name;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "user_device_use",
            joinColumns = @JoinColumn(name = "UUID_device"),
            inverseJoinColumns = @JoinColumn(name = "user_internal_id")
    )
    @NonNull
    @ToString.Exclude
    @OrderBy
    private Set<User> users = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "possible_buyer_user_device",
            joinColumns = @JoinColumn(name = "UUID_dispositivo"),
            inverseJoinColumns = @JoinColumn(name = "user_internal_id")
    )
    @NonNull
    @ToString.Exclude
    @OrderBy
    private Set<User> possibleBuyers = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "buyer_user_id")
    private User buyer;

    public Set<User> addUser(User userToAdd) {
        this.users.add(userToAdd);
        userToAdd.getUsingDevices().add(this);
        return this.users;
    }

    public Set<User> removeUser(User userToRemove) {
        this.users.remove(userToRemove);
        userToRemove.getUsingDevices().remove(this);
        return this.users;
    }

    public Set<User> addPossibleBuyer(User possibleBuyer) {
        this.possibleBuyers.add(possibleBuyer);
        possibleBuyer.getMayBuyFor().add(this);
        return this.possibleBuyers;
    }

    public Set<User> removePossibleBuyer(User userToRemove) {
        this.possibleBuyers.remove(userToRemove);
        userToRemove.getMayBuyFor().remove(this);
        return this.possibleBuyers;
    }

    public void establishBuyer(User buyer) {
        this.buyer = buyer;
        buyer.getBuysFor().add(this);
    }

    public void removeBuyer(User buyer) {
        this.buyer = null;
        buyer.getBuysFor().remove(this);
    }

}
