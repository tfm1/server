package com.university.dferreiropresedo.tfm.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "user")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User implements Serializable {

    @Id
    @Column(name = "user_internal_id")
    @NonNull
    @EqualsAndHashCode.Include
    private String internalId;

    @Column(nullable = false)
    @NonNull
    @EqualsAndHashCode.Include
    private String name;

    @Column(nullable = false)
    @NonNull
    @EqualsAndHashCode.Include
    private String email;

    @Column(name = "is_admin", nullable = false)
    @EqualsAndHashCode.Include
    private boolean isAdmin = false;

    @Column(name = "image_url")
    @EqualsAndHashCode.Include
    private String imageUrl = null;

    @ManyToMany(mappedBy = "users", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @NonNull
    @ToString.Exclude
    @OrderBy
    private Set<Device> usingDevices = new HashSet<>();

    @ManyToMany(mappedBy = "possibleBuyers", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @NonNull
    @ToString.Exclude
    @OrderBy
    private Set<Device> mayBuyFor = new HashSet<>();

    @OneToMany(mappedBy = "buyer")
    @NonNull
    @ToString.Exclude
    @OrderBy
    private Set<Device> buysFor = new HashSet<>();

}
