package com.university.dferreiropresedo.tfm.controller;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.RegisterDTO;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/inventory", produces = MediaType.APPLICATION_JSON_VALUE)
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private UserService userService;

    @GetMapping(path = "/register")
    public List<RegisterDTO> getAllDeviceProducts(@RequestParam(value = "device") String deviceUUID) {

        if (deviceUUID == null || deviceUUID.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The device parameter is mandatory");
        }

        List<Register> deviceRegisters = inventoryService.retrieveDeviceRegisters(deviceUUID);

        return deviceRegisters.stream().map(RegisterDTO::fromEntity).collect(Collectors.toList());
    }


    @PostMapping(path = "/register")
    public List<RegisterDTO> updateRegistersInfo(@RequestBody List<RegisterDTO> infoToUpdate, Principal userEmail) {

        if (infoToUpdate == null || infoToUpdate.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The registers are mandatory");
        }

        Optional<User> optUserFound = userService.retrieveUserInformation(userEmail.getName());
        if (!optUserFound.isPresent()) {
            throw new ServerException(ErrorCode.USER_NOT_FOUND, "The user couldn't be found");
        }

        List<Register> registersToUpdate = infoToUpdate.stream().map(RegisterDTO::toRegister).collect(Collectors.toList());
        List<Register> registersUpdate = inventoryService.updateRegistersInfo(registersToUpdate, optUserFound.get());
        return RegisterDTO.fromEntity(registersUpdate);
    }

}
