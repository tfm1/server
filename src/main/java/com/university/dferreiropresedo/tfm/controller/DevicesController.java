package com.university.dferreiropresedo.tfm.controller;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.UserDTO;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/devices", produces = MediaType.APPLICATION_JSON_VALUE)
public class DevicesController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private UserService userService;


    @GetMapping("/user")
    public List<UserDTO> getAllDeviceUsers(@RequestParam(value = "device") String deviceUUID, Principal principal) {
        if (deviceUUID == null || deviceUUID.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The device parameter is mandatory");
        }

        Set<User> deviceUsers = this.deviceService.getAllDeviceUsers(deviceUUID, principal.getName());
        return deviceUsers.stream().map(UserDTO::fromUser).collect(Collectors.toList());
    }

    @PostMapping("/user")
    public void addUsersToDevice(@RequestParam(value = "device") String deviceUUID, @RequestBody List<String> usersToAdd, Principal principal) {
        if (deviceUUID == null || deviceUUID.isEmpty() || usersToAdd == null || usersToAdd.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The device and users parameters are mandatory");
        }

        this.deviceService.connectUsersToDevice(deviceUUID, usersToAdd, principal.getName());
    }


    @DeleteMapping(path = "/user")
    public void removeUsersFromDevice(@RequestParam(value = "device") String deviceUUID, @RequestBody List<UserDTO> usersSendToRemove, Principal principal) {
        if (deviceUUID == null || deviceUUID.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The device parameter is mandatory");
        }

        if (usersSendToRemove == null || usersSendToRemove.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The users parameter is mandatory");
        }

        List<User> usersToRemove = usersSendToRemove.stream().map(UserDTO::toUser).collect(Collectors.toList());
        this.deviceService.removeUsersFromDevice(deviceUUID,usersToRemove, principal.getName());
    }

}
