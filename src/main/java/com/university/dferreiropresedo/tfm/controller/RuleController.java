package com.university.dferreiropresedo.tfm.controller;


import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.DeviceRulesDTO;
import com.university.dferreiropresedo.tfm.dto.RuleDTO;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.services.rule.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/rules", produces = MediaType.APPLICATION_JSON_VALUE)
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @GetMapping(path = "/rule")
    public List<RuleDTO> retrieveInventoryRules(@RequestParam(value = "inventory") Long inventoryId, Principal principal) {

        if (inventoryId == null || inventoryId == 0L) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(), "The inventory field is mandatory");
        }

        List<Rule> inventoryRules = ruleService.retrieveInventoryRules(inventoryId, principal.getName());

        return RuleDTO.fromEntity(inventoryRules);
    }

    @PostMapping(path = "/rule")
    public List<RuleDTO> createNewRule(@RequestBody DeviceRulesDTO deviceRules, Principal principal) {
        if (deviceRules == null) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(), "The deviceRules field is mandatory");
        } else {
            Device device = deviceRules.getDeviceDTO().toDevice();
            List<Rule> rules = deviceRules.getRules().stream().map(rule -> rule.toRule(device)).collect(Collectors.toList());
            List<Rule> rulesCreated = this.ruleService.createRules(rules, principal.getName());
            return RuleDTO.fromEntity(rulesCreated);
        }
    }


    @PutMapping(path = "/rule")
    public void saveRuleChanges(@RequestBody DeviceRulesDTO rulesChanges, Principal principal) {

        if (rulesChanges == null || rulesChanges.getRules() == null || rulesChanges.getRules().isEmpty() || rulesChanges.getDeviceDTO() == null) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(), "The changed rules field is mandatory");
        }


        Device device = rulesChanges.getDeviceDTO().toDevice();
        List<Rule> rules = rulesChanges.getRules().stream().map(rule -> rule.toRule(device)).collect(Collectors.toList());
        ruleService.updateRules(device, rules, principal.getName());

    }

    @DeleteMapping(path = "/rule")
    public void removeRules(@RequestBody List<Long> rulesId, Principal principal) {
        if (rulesId == null || rulesId.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(), "The rules ids are mandatory");
        } else {
            this.ruleService.removeRules(rulesId, principal.getName());
        }
    }

}
