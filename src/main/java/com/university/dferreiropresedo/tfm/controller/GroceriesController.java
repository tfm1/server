package com.university.dferreiropresedo.tfm.controller;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.RegisterDTO;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.services.groceries.GroceriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/groceries", produces = MediaType.APPLICATION_JSON_VALUE)
public class GroceriesController {

    @Autowired
    private GroceriesService groceriesService;

    @GetMapping(path = "/list")
    public List<RegisterDTO> requestGroceriesList(@RequestParam("deviceUUID") String deviceUUID,
                                                  @RequestParam("deviceUUID") String deviceName, Principal user) {

        if (deviceUUID == null || deviceUUID.isEmpty() || deviceName == null || deviceName.isEmpty()) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The device parameter is mandatory");
        }

        List<Register> groceriesList = groceriesService.requestGroceriesList(new Device(deviceUUID, deviceName), user.getName());
        return RegisterDTO.fromEntity(groceriesList);
    }


}
