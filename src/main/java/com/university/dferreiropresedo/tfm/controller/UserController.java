package com.university.dferreiropresedo.tfm.controller;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.dto.DeviceDTO;
import com.university.dferreiropresedo.tfm.dto.DeviceInformationDTO;
import com.university.dferreiropresedo.tfm.dto.InventoryDTO;
import com.university.dferreiropresedo.tfm.dto.UserDTO;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.services.AuthenticationService;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    public final static String TOKEN_PREFIX_OAUTH = "Bearer";
    public final static String TOKEN_PREFIX_BASIC = "Basic";

    @Autowired
    private AuthenticationService authService;

    @Autowired
    private UserService userService;

    @Autowired
    private DeviceService deviceService;

    @GetMapping("/register")
    public UserDTO authenticateUser(@RequestHeader(HttpHeaders.AUTHORIZATION) String authorization) {
        if (authorization == null || !authorization.startsWith(TOKEN_PREFIX_OAUTH)) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new IllegalArgumentException(),
                    "The authorization header is mandatory or it does not have the right format");
        }

        String accessToken = authorization.split(" ")[1];
        User user = this.authService.retrieveUserInformationFromAccessToken(accessToken);
        if (user == null) {
            throw new ServerException(ErrorCode.SIGN_IN_FAILED, new IllegalArgumentException(), "The given token is invalid");
        }
        return UserDTO.fromUser(this.userService.registerUser(user));
    }

    @DeleteMapping(path = "/devices")
    public void disconnectUserDevice(@RequestBody List<DeviceDTO> devicesSelected, Principal principal) {
        if (devicesSelected != null && !devicesSelected.isEmpty()) {
            List<Device> userSelectedDevices = devicesSelected.stream().map(DeviceDTO::toDevice).collect(Collectors.toList());
            this.deviceService.disconnectUserDevices(userSelectedDevices, principal.getName());
        } else {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(), "The selected devices field is mandatory");
        }
    }

    @PostMapping(path = "/devices")
    public DeviceInformationDTO connectUserDevice(@RequestBody DeviceDTO deviceToConnect, Principal principal) {
        if (deviceToConnect == null) {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, new InvalidParameterException(),
                    "The selected devices field is mandatory");
        } else {
            Inventory inventoryAssociated = this.deviceService.connectUserDevice(deviceToConnect.toDevice(), principal.getName());
            return new DeviceInformationDTO(DeviceDTO.fromDevice(inventoryAssociated.getDevice()),
                    InventoryDTO.fromInventory(inventoryAssociated));
        }
    }

    @PostMapping("/users")
    public void upgradeUsers(@RequestBody List<UserDTO> usersToUpgrade, Principal principal) {
        if (usersToUpgrade != null && !usersToUpgrade.isEmpty()) {
            List<User> users = usersToUpgrade.stream().map(UserDTO::toUser).collect(Collectors.toList());
            this.userService.upgradeUsers(users, principal.getName());

        } else {
            throw new ServerException(ErrorCode.INVALID_PARAMETER, "The users to be upgraded must be a valid field");
        }
    }

    @GetMapping("/users")
    public List<UserDTO> retrieveAllPossibleUsers(@RequestParam("users") List<String> usersId, Principal principal) {
        List<User> users = this.userService.retrievePossibleUsersToAdd(usersId, principal.getName());
        return users.stream().map(UserDTO::fromUser).collect(Collectors.toList());
    }


}
