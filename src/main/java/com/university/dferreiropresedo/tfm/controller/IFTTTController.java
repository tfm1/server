package com.university.dferreiropresedo.tfm.controller;


import com.university.dferreiropresedo.tfm.services.voiceservice.VoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/ifttt", produces = MediaType.APPLICATION_JSON_VALUE)
public class IFTTTController {

    private final static List<String> POST_REMOVE_WORDS = Arrays.asList("\bdespensa","\"");
    private final static List<String> POST_REMOVE_PREPOSITIONS = Arrays.asList("\\b(de|en|la|a|mi)\\b");

    @Autowired
    private VoiceService voiceService;

    @PostMapping(path = "/add")
    public void addStockOperation(@RequestParam(name = "product") String productName, @RequestParam(name = "quantity") Double quantity,
                                  Principal principal) {

        String productSanitized = sanitizeVoiceOperation(productName);

        voiceService.addProduct(productSanitized, quantity, principal.getName());
    }

    @PostMapping(path = "/remove")
    public void withdrawStockOperation(@RequestParam(name = "product") String productName, @RequestParam(name = "quantity") Double quantity,
                                       Principal principal) {

        String productSanitized = sanitizeVoiceOperation(productName);
        voiceService.withdrawProduct(productSanitized, quantity, principal.getName());
    }

    private String sanitizeVoiceOperation(String said) {
        String afterRemoval = said.trim();
        for (String removal : POST_REMOVE_WORDS) {
            afterRemoval = afterRemoval.replaceAll(removal, "").trim();
        }
        for (String removal : POST_REMOVE_PREPOSITIONS) {
            afterRemoval = afterRemoval.replaceAll(removal, "").trim();
        }
        return afterRemoval.trim();
    }

}
