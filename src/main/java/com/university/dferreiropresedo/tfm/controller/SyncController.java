package com.university.dferreiropresedo.tfm.controller;

import com.university.dferreiropresedo.tfm.dto.DeviceInformationDTO;
import com.university.dferreiropresedo.tfm.dto.UserDeviceInformationDTO;
import com.university.dferreiropresedo.tfm.services.sync.SyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "/sync", produces = MediaType.APPLICATION_JSON_VALUE)
public class SyncController {

    @Autowired
    private SyncService syncService;

    @GetMapping(path = "/devices")
    public List<UserDeviceInformationDTO> syncUserInformation(Principal principal) {
        return syncService.syncDeviceInformation(principal.getName());
    }

}
