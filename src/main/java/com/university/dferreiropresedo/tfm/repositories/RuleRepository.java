package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RuleRepository extends CrudRepository<Rule, Long> {
}
