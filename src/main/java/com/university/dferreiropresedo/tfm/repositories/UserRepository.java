package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByInternalId(String userInternalId);

    User findByEmail(String email);

    List<User> findByEmailIn(List<String> email);

    List<User> findByInternalIdNotIn(List<String> internalIds);
}
