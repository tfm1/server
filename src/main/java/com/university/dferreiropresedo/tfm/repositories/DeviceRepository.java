package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.Device;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends CrudRepository<Device, String> {

    List<Device> findDevicesByUsers_internalIdLikeAndUUIDIn(String userInternalId, List<String> uuid);

    List<Device> findDevicesByUsers_internalIdLike(String userInternalId);

}
