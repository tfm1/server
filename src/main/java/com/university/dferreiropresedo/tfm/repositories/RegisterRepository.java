package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RegisterRepository extends CrudRepository<Register, Long> {

    List<Register> findRegistersByInventory_idAndProduct_idIn(Long inventoryId, List<Long> productIds);

}
