package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryRepository extends CrudRepository<Inventory, Long> {

    Inventory findByDeviceUUID(String deviceUUID);

    List<Inventory> findAllByDeviceUUIDIn(List<String> devicesUUID);

}
