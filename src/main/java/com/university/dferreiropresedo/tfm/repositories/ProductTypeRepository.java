package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.ProductType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductTypeRepository extends CrudRepository<ProductType, Long> {
}
