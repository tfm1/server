package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    Product findByTag(String tag);
}
