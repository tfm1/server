package com.university.dferreiropresedo.tfm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = {
        "com.university.dferreiropresedo.tfm.repositories",
        "com.university.dferreiropresedo.tfm.entities"
})
@PropertySource("classpath:application-test.properties")
@EnableTransactionManagement
public class H2TestingDatabaseConfig {
}
