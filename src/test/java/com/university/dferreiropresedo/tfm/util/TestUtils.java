package com.university.dferreiropresedo.tfm.util;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.ProductType;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;

public class TestUtils {

    public static User generateRandomUser() {

        return new User(BasicRandomGenerator.generateRandomString(), BasicRandomGenerator.generateRandomString(),
                BasicRandomGenerator.generateRandomString());
    }

    public static ProductType generateRandomProductType() {

        return new ProductType(BasicRandomGenerator.generateRandomString(), BasicRandomGenerator.generateRandomString());
    }

    public static Product generateRandomProduct() {

        Product p = new Product(BasicRandomGenerator.generateRandomString(),BasicRandomGenerator.generateRandomString(),
                generateRandomProductType());
        p.setId(BasicRandomGenerator.generateRandomLong());
        return p;
    }

    public static Product generateRandomProductWithoutId() {
        return new Product(BasicRandomGenerator.generateRandomString(), BasicRandomGenerator.generateRandomString(),
                generateRandomProductType());
    }

    public static Device generateRandomDevice() {
        return new Device(BasicRandomGenerator.generateRandomString(36), BasicRandomGenerator.generateRandomString());
    }

    public static Inventory generateRandomInventory() {
        return new Inventory(generateRandomDevice());
    }

    public static Register generateRandomRegister() {
        return new Register(generateRandomInventory(), generateRandomProduct(), BasicRandomGenerator.generateRandomDouble());
    }

    public static Rule generateRandomRule() {
        return new Rule(generateRandomInventory(), generateRandomProduct(), BasicRandomGenerator.generateRandomDouble());
    }

}














