package com.university.dferreiropresedo.tfm.util;

import java.util.Random;

public class BasicRandomGenerator {

    public static long generateRandomLong() {
        long leftLimit = 1L;
        long rightLimit = 100L;
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    public static double generateRandomDouble() {
        double leftLimit = 0D;
        double rightLimit = 100D;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    public static int generateRandomInt() {
        return new Random().ints(1, 1, 20).findFirst().getAsInt();
    }

    public static String generateRandomString() {
        return generateRandomString(generateRandomInt());
    }

    public static String generateRandomString(int length) {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
}
