package com.university.dferreiropresedo.tfm.services.inventory;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.repositories.InventoryRepository;
import com.university.dferreiropresedo.tfm.repositories.RegisterRepository;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class InventoryServiceTest {

    @Mock
    InventoryRepository inventoryRepository;

    @Mock
    RegisterRepository registerRepository;

    @InjectMocks
    InventoryService inventoryService = new InventoryServiceImpl();

    @Captor
    ArgumentCaptor<List<Register>> registerListCaptor;

    @Captor
    ArgumentCaptor<Inventory> inventoryCaptor;

    @Test
    public void whenUpdateRegistersInfoNonexistentInventory_ThenReturnException() {

        User userCreated = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated1 = TestUtils.generateRandomRegister();
        inventoryCreated.setDevice(deviceCreated);
        inventoryCreated.addRegister(registerCreated1);
        registerCreated1.setInventory(inventoryCreated);

        Mockito.doReturn(Optional.empty()).when(this.inventoryRepository).findById(inventoryCreated.getId());

        ServerException exceptionCaptured = assertThrows(ServerException.class, () -> this.inventoryService.updateRegistersInfo(Collections.singletonList(registerCreated1), userCreated));

        assertThat(exceptionCaptured.getErrorCode()).isEqualTo(ErrorCode.GENERAL_ERROR);
    }

    @Test
    public void whenUpdateRegistersInfoWithUserNotConnected_ThenReturnException() {

        User userCreated = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated1 = TestUtils.generateRandomRegister();

        inventoryCreated.setDevice(deviceCreated);
        registerCreated1.setInventory(inventoryCreated);
        inventoryCreated.addRegister(registerCreated1);

        Mockito.doReturn(Optional.of(inventoryCreated)).when(this.inventoryRepository).findById(inventoryCreated.getId());

        ServerException exceptionCaptured = assertThrows(ServerException.class, () -> this.inventoryService
                .updateRegistersInfo(Collections.singletonList(registerCreated1), userCreated));

        assertThat(exceptionCaptured.getErrorCode()).isEqualTo(ErrorCode.USER_NOT_PRIVILEGED);
    }

    @Test
    public void whenUpdateRegistersInfo_ThenRegistersAreUpdated() {

        Product productCreated1 = TestUtils.generateRandomProduct();
        Product productCreated2 = TestUtils.generateRandomProduct();
        Product productCreated3 = TestUtils.generateRandomProduct();
        Product productCreated4 = TestUtils.generateRandomProduct();
        Register registerCreated1 = TestUtils.generateRandomRegister();
        registerCreated1.setProduct(productCreated1);
        Register registerCreated2 = TestUtils.generateRandomRegister();
        registerCreated2.setProduct(productCreated2);
        Register registerCreated3 = TestUtils.generateRandomRegister();
        registerCreated3.setProduct(productCreated3);
        Register registerCreated4 = TestUtils.generateRandomRegister();
        registerCreated4.setProduct(productCreated4);
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        inventoryCreated.addRegister(registerCreated1);
        inventoryCreated.addRegister(registerCreated2);
        inventoryCreated.addRegister(registerCreated3);
        Device deviceCreated = TestUtils.generateRandomDevice();
        inventoryCreated.setDevice(deviceCreated);
        User userCreated = TestUtils.generateRandomUser();
        deviceCreated.addUser(userCreated);

        List<Long> productsIds = Arrays.asList(productCreated1.getId(), productCreated2.getId(), productCreated3.getId(), productCreated4.getId());
        List<Register> registersAlreadyAssociated = Arrays.asList(registerCreated1, registerCreated2, registerCreated3);

        Register registerToDelete = new Register(registerCreated1.getInventory(), registerCreated1.getProduct(), 0D);
        Register registerToUpdate = new Register(registerCreated2.getInventory(), registerCreated2.getProduct(), registerCreated2.getAmount() + 10D);

        List<Register> inputData = Arrays.asList(registerToDelete, registerToUpdate, registerCreated3, registerCreated4);

        Mockito.doReturn(Optional.of(inventoryCreated)).when(this.inventoryRepository).findById(inventoryCreated.getId());
        Mockito.doReturn(registersAlreadyAssociated)
                .when(this.registerRepository).findRegistersByInventory_idAndProduct_idIn(
                        inventoryCreated.getId(), productsIds);

        this.inventoryService.updateRegistersInfo(inputData, userCreated);

        Mockito.verify(this.registerRepository).deleteAll(registerListCaptor.capture());
        List<Register> registersToBeDeleted = registerListCaptor.getValue();
        assertThat(registersToBeDeleted).containsOnly(registerCreated1);

        Mockito.verify(this.registerRepository, Mockito.times(2)).saveAll(registerListCaptor.capture());
        List<List<Register>> allValuesCaptured = registerListCaptor.getAllValues();
        assertThat(allValuesCaptured.get(1)).contains(registerToUpdate);
        assertThat(allValuesCaptured.get(2)).containsOnly(registerCreated4);
    }
}