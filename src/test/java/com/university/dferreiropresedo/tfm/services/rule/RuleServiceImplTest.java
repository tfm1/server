package com.university.dferreiropresedo.tfm.services.rule;

import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.repositories.InventoryRepository;
import com.university.dferreiropresedo.tfm.repositories.RuleRepository;
import com.university.dferreiropresedo.tfm.services.device.DeviceService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class RuleServiceImplTest {

    @Mock
    UserService userService;

    @Mock
    DeviceService deviceService;

    @Mock
    RuleRepository ruleRepository;

    @Mock
    InventoryRepository inventoryRepository;

    @InjectMocks
    RuleService ruleService = new RuleServiceImpl();

    @Captor
    ArgumentCaptor<List<Rule>> ruleCaptor;

    @Captor
    ArgumentCaptor<Inventory> inventoryCaptor;

    @Test
    void whenRulesUpdated_ThenRulesWillUpdated() {

        Product productCreated1 = TestUtils.generateRandomProduct();
        Product productCreated2 = TestUtils.generateRandomProduct();
        Product productCreated3 = TestUtils.generateRandomProduct();
        Product productCreated4 = TestUtils.generateRandomProduct();
        User userCreated = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Rule ruleCreated1 = TestUtils.generateRandomRule();
        Rule ruleCreated2 = TestUtils.generateRandomRule();
        Rule ruleCreated3 = TestUtils.generateRandomRule();
        Rule ruleCreated4 = TestUtils.generateRandomRule();

        deviceCreated.addUser(userCreated);
        inventoryCreated.setDevice(deviceCreated);
        ruleCreated1.setProduct(productCreated1);
        ruleCreated2.setProduct(productCreated2);
        ruleCreated3.setProduct(productCreated3);
        ruleCreated4.setProduct(productCreated4);

        inventoryCreated.addRule(ruleCreated1);
        inventoryCreated.addRule(ruleCreated2);
        ruleCreated1.setInventory(inventoryCreated);
        ruleCreated2.setInventory(inventoryCreated);

        Rule ruleUpdated = new Rule(ruleCreated1.getInventory(), ruleCreated1.getProduct(),
                ruleCreated1.getAmount() + 10D);

        Mockito.doReturn(Optional.of(userCreated)).when(this.userService).retrieveUserInformation(userCreated.getEmail());
        Mockito.doReturn(inventoryCreated).when(this.deviceService).retrieveDeviceInventory(deviceCreated, userCreated.getEmail());

        List<Rule> inputRules = Arrays.asList(ruleUpdated, ruleCreated3, ruleCreated4);

        this.ruleService.updateRules(deviceCreated,inputRules,userCreated.getEmail());

        Mockito.verify(this.ruleRepository, Mockito.times(2)).saveAll(ruleCaptor.capture());
        List<List<Rule>> rulesCaptured = ruleCaptor.getAllValues();
        assertThat(rulesCaptured.get(0)).containsOnly(ruleUpdated);
        assertThat(rulesCaptured.get(1)).containsOnly(ruleCreated3, ruleCreated4);

        Mockito.verify(this.inventoryRepository).save(inventoryCaptor.capture());
        Inventory inventoryCaptured = inventoryCaptor.getValue();

        assertThat(inventoryCaptured.getRules()).containsOnly(ruleUpdated, ruleCreated3, ruleCreated4, ruleCreated2);

    }
}