package com.university.dferreiropresedo.tfm.services.device;

import com.university.dferreiropresedo.tfm.configurations.exceptions.ErrorCode;
import com.university.dferreiropresedo.tfm.configurations.exceptions.ServerException;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.repositories.DeviceRepository;
import com.university.dferreiropresedo.tfm.services.inventory.InventoryService;
import com.university.dferreiropresedo.tfm.services.user.UserService;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class DeviceServiceImplTest {

    @Mock
    UserService userService;

    @Mock
    InventoryService inventoryService;

    @Mock
    DeviceRepository deviceRepository;

    @InjectMocks
    DeviceService deviceService = new DeviceServiceImpl();

    @Captor
    ArgumentCaptor<Device> deviceArgumentCaptor;

    @Captor
    ArgumentCaptor<List<Device>> deviceListArgumentCaptor;

    @Test
    public void whenRetrieveDeviceInventory_ThenCorrectInventoryIsReturned() {
        User userCreated = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        deviceCreated.addUser(userCreated);
        inventoryCreated.setDevice(deviceCreated);

        Mockito.doReturn(Optional.of(userCreated)).when(this.userService).retrieveUserInformation(userCreated.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        Mockito.doReturn(inventoryCreated).when(this.inventoryService).retrieveInventoryData(deviceCreated);
        Inventory inventoryFound = this.deviceService.retrieveDeviceInventory(deviceCreated, userCreated.getEmail());

        assertThat(inventoryFound).isEqualTo(inventoryCreated);
    }

    @Test
    public void whenRetrieveDeviceInventoryWithNonexistentUser_ThenReturnNull() {
        User userCreated = TestUtils.generateRandomUser();
        User userCreated2 = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        deviceCreated.addUser(userCreated);
        inventoryCreated.setDevice(deviceCreated);

        Mockito.doReturn(Optional.empty()).when(this.userService).retrieveUserInformation(userCreated2.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        Inventory inventoryFound = this.deviceService.retrieveDeviceInventory(deviceCreated, userCreated2.getEmail());

        assertThat(inventoryFound).isNull();
    }

    @Test
    public void whenRemoveUsersFromDevice_UsersAreRemoved() {

        Device deviceCreated = TestUtils.generateRandomDevice();
        User userCreated = TestUtils.generateRandomUser();
        User userCreated2 = TestUtils.generateRandomUser();
        User userCreated3 = TestUtils.generateRandomUser();
        User adminUser = TestUtils.generateRandomUser();
        adminUser.setAdmin(true);
        deviceCreated.addUser(userCreated);
        deviceCreated.addUser(userCreated2);
        deviceCreated.addUser(userCreated3);

        Mockito.doReturn(Optional.of(adminUser)).when(this.userService).retrieveUserInformation(adminUser.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        this.deviceService.removeUsersFromDevice(deviceCreated.getUUID(), Arrays.asList(userCreated, userCreated2), adminUser.getEmail());

        Mockito.verify(this.deviceRepository).save(deviceArgumentCaptor.capture());
        Device deviceToBeSaved = deviceArgumentCaptor.getValue();
        assertThat(deviceToBeSaved.getUsers()).containsOnly(userCreated3);
    }

    @Test
    public void whenRemoveUsersFromDeviceNotAdminUser_ThenException() {

        Device deviceCreated = TestUtils.generateRandomDevice();
        User userCreated = TestUtils.generateRandomUser();
        User userCreated2 = TestUtils.generateRandomUser();
        User userCreated3 = TestUtils.generateRandomUser();
        User notAdminUser = TestUtils.generateRandomUser();
        notAdminUser.setAdmin(false);
        deviceCreated.addUser(userCreated);
        deviceCreated.addUser(userCreated2);
        deviceCreated.addUser(userCreated3);
        Mockito.doReturn(Optional.of(notAdminUser)).when(this.userService).retrieveUserInformation(notAdminUser.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());

        ServerException exceptionCaptured = assertThrows(ServerException.class, () -> {
            this.deviceService.removeUsersFromDevice(deviceCreated.getUUID(), Arrays.asList(userCreated, userCreated2),
                    notAdminUser.getEmail());
        });

        assertThat(exceptionCaptured.getErrorCode()).isEqualByComparingTo(ErrorCode.USER_NOT_PRIVILEGED);
    }

    @Test
    public void whenUserDisconnectedFromDevice_ThenUserRemainDisconnected() {

        Device deviceCreated = TestUtils.generateRandomDevice();
        User userCreated = TestUtils.generateRandomUser();
        deviceCreated.addUser(userCreated);
        Mockito.doReturn(Optional.of(userCreated)).when(this.userService).retrieveUserInformation(userCreated.getEmail());
        Mockito.doReturn(Collections.singletonList(deviceCreated)).when(this.deviceRepository)
                .findDevicesByUsers_internalIdLikeAndUUIDIn(userCreated.getInternalId(),
                        Collections.singletonList(deviceCreated.getUUID()));

        this.deviceService.disconnectUserDevices(Collections.singletonList(deviceCreated), userCreated.getEmail());

        Mockito.verify(this.deviceRepository).saveAll(deviceListArgumentCaptor.capture());
        List<Device> devicesUpdated = deviceListArgumentCaptor.getValue();

        assertThat(devicesUpdated).allMatch(device -> !device.getUsers().contains(userCreated));
    }

    @Test
    public void whenUserConnectDevices_ThenReturnsTheInventoryAssociated() {

        User createdUser = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        inventoryCreated.setDevice(deviceCreated);

        Mockito.doReturn(Optional.of(createdUser)).when(this.userService).retrieveUserInformation(createdUser.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        Mockito.doReturn(deviceCreated).when(this.deviceRepository).save(deviceCreated);
        Mockito.doReturn(inventoryCreated).when(this.inventoryService).retrieveInventoryData(deviceCreated);

        Inventory inventoryAssociated = this.deviceService.connectUserDevice(deviceCreated, createdUser.getEmail());

        assertThat(inventoryAssociated).isEqualTo(inventoryCreated);
    }

    @Test
    public void whenUserConnectsToEmptyDevice_ThenReturnsTheEmptyInventory() {

        User createdUser = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory emptyInventoryCreated = TestUtils.generateRandomInventory();

        Mockito.doReturn(Optional.of(createdUser)).when(this.userService).retrieveUserInformation(createdUser.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        Mockito.doReturn(deviceCreated).when(this.deviceRepository).save(deviceCreated);
        Mockito.doReturn(null).when(this.inventoryService).retrieveInventoryData(deviceCreated);
        Mockito.doReturn(emptyInventoryCreated).when(this.inventoryService).associateEmptyInventoryToDevice(deviceCreated);

        Inventory emptyInventory = this.deviceService.connectUserDevice(deviceCreated, createdUser.getEmail());

        assertThat(emptyInventory).isEqualTo(emptyInventoryCreated);
    }

    @Test
    public void whenAdminConnectsFewUsersToDevice_ThenDeviceHasSaidUsers() {

        User adminUser = TestUtils.generateRandomUser();
        adminUser.setAdmin(true);
        User userCreated = TestUtils.generateRandomUser();
        User userCreated2 = TestUtils.generateRandomUser();
        User userCreated3 = TestUtils.generateRandomUser();
        User userCreated4 = TestUtils.generateRandomUser();
        Device deviceCreated = TestUtils.generateRandomDevice();
        deviceCreated.addUser(userCreated2);
        deviceCreated.addUser(userCreated4);

        Mockito.doReturn(Optional.of(adminUser)).when(this.userService).retrieveUserInformation(adminUser.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());
        Mockito.doReturn(Arrays.asList(userCreated, userCreated3)).when(this.userService).retrieveUsersInformation(Arrays.asList(userCreated.getEmail(), userCreated3.getEmail()));

        this.deviceService.connectUsersToDevice(deviceCreated.getUUID(), Arrays.asList(userCreated.getEmail(), userCreated3.getEmail()), adminUser.getEmail());

        Mockito.verify(this.deviceRepository).save(deviceArgumentCaptor.capture());
        Device deviceToBeSaved = deviceArgumentCaptor.getValue();

        assertThat(deviceToBeSaved.getUsers()).contains(userCreated, userCreated2, userCreated3, userCreated4);
    }

    @Test
    public void whenNotAdminTriesToConnectUsersToDevice_ThenException() {

        User userCreated = TestUtils.generateRandomUser();
        userCreated.setAdmin(false);
        Device deviceCreated = TestUtils.generateRandomDevice();

        Mockito.doReturn(Optional.of(userCreated)).when(this.userService).retrieveUserInformation(userCreated.getEmail());
        Mockito.doReturn(Optional.of(deviceCreated)).when(this.deviceRepository).findById(deviceCreated.getUUID());

        ServerException exceptionCaptured = assertThrows(ServerException.class, () -> this.deviceService.connectUsersToDevice(deviceCreated.getUUID(), Collections.emptyList(), userCreated.getEmail()));

        assertThat(exceptionCaptured.getErrorCode()).isEqualTo(ErrorCode.USER_NOT_PRIVILEGED);
    }
}