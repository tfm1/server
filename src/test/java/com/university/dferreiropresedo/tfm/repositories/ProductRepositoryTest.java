package com.university.dferreiropresedo.tfm.repositories;


import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.ProductType;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
class ProductRepositoryTest {

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Autowired
    private ProductRepository productRepository;


    @Test
    public void whenSavedAndFindById_ThenReturnProduct() {

        ProductType productTypeCreated = TestUtils.generateRandomProductType();
        Product productCreated = TestUtils.generateRandomProductWithoutId();
        productCreated.setProductType(productTypeCreated);
        productTypeCreated.addProduct(productCreated);

        this.productTypeRepository.save(productTypeCreated);
        Optional<Product> productFound = this.productRepository.findById(productCreated.getId());
        Optional<ProductType> productTypeFound = this.productTypeRepository.findById(productTypeCreated.getId());

        assertThat(productFound.isPresent()).isTrue();

        Product productToCompare = productFound.orElse(new Product());
        ProductType productTypeToCompare = productTypeFound.orElse(new ProductType());

        assertThat(productToCompare.getName()).isEqualTo(productCreated.getName());
        assertThat(productToCompare.getId()).isEqualTo(productCreated.getId());
        assertThat(productTypeToCompare.getName()).isEqualTo(productTypeCreated.getName());
        assertThat(productTypeToCompare.getId()).isEqualTo(productTypeCreated.getId());
        assertThat(productToCompare.getProductType()).isEqualTo(productTypeCreated);
        assertThat(productTypeToCompare.getProducts()).contains(productCreated);
    }

}