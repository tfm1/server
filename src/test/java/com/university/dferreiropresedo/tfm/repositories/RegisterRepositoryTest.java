package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
public class RegisterRepositoryTest {

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Test
    public void whenSavedRegisterFindById_ThenRegisterReturned() {

        Product productCreated = TestUtils.generateRandomProductWithoutId();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated = TestUtils.generateRandomRegister();
        registerCreated.setInventory(inventoryCreated);
        registerCreated.setProduct(productCreated);

        inventoryCreated.addRegister(registerCreated);

        this.deviceRepository.save(inventoryCreated.getDevice());
        this.productTypeRepository.save(productCreated.getProductType());
        this.productRepository.save(productCreated);
        this.inventoryRepository.save(inventoryCreated);
        this.registerRepository.save(registerCreated);

        Optional<Register> registerFound = this.registerRepository.findById(registerCreated.getId());
        Optional<Inventory> inventoryFound = this.inventoryRepository.findById(inventoryCreated.getId());
        Optional<Product> productFound = this.productRepository.findById(productCreated.getId());

        assertTrue(registerFound.isPresent());
        assertTrue(inventoryFound.isPresent());
        assertTrue(productFound.isPresent());

        Register registerTest = registerFound.orElse(new Register());
        Inventory inventoryTest = inventoryFound.orElse(new Inventory());
        Product productTest = productFound.orElse(new Product());

        assertEquals(registerCreated.getId(), registerTest.getId());
        assertEquals(registerCreated.getAmount(), registerTest.getAmount());
        assertEquals(inventoryCreated.getId(), inventoryTest.getId());
        assertEquals(productCreated.getId(), productTest.getId());
        assertThat(inventoryTest).isEqualToComparingFieldByField(registerTest.getInventory());
        assertThat(productTest).isEqualToComparingFieldByField(registerTest.getProduct());
        assertThat(inventoryTest.getRegisters()).contains(registerTest);
    }

    @Test
    public void whenFindByInventoryIdAndProductIds_ThenCorrectRegistersAreReturned() {

        Product productCreated1 = TestUtils.generateRandomProductWithoutId();
        Product productCreated2 = TestUtils.generateRandomProductWithoutId();
        Product productCreated3 = TestUtils.generateRandomProductWithoutId();
        Product productCreated4 = TestUtils.generateRandomProductWithoutId();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated1 = TestUtils.generateRandomRegister();
        Register registerCreated2 = TestUtils.generateRandomRegister();
        Register registerCreated3 = TestUtils.generateRandomRegister();
        Register registerCreated4 = TestUtils.generateRandomRegister();
        inventoryCreated.setDevice(deviceCreated);
        registerCreated1.setProduct(productCreated1);
        registerCreated2.setProduct(productCreated2);
        registerCreated3.setProduct(productCreated3);
        registerCreated4.setProduct(productCreated4);

        this.productTypeRepository.save(productCreated1.getProductType());
        this.productTypeRepository.save(productCreated2.getProductType());
        this.productTypeRepository.save(productCreated3.getProductType());
        this.productTypeRepository.save(productCreated4.getProductType());
        Product productSaved1 = this.productRepository.save(productCreated1);
        Product productSaved2 = this.productRepository.save(productCreated2);
        Product productSaved3 = this.productRepository.save(productCreated3);
        Product productSaved4 = this.productRepository.save(productCreated4);
        Device deviceSaved = this.deviceRepository.save(inventoryCreated.getDevice());
        inventoryCreated.setDevice(deviceSaved);
        Inventory inventorySaved = this.inventoryRepository.save(inventoryCreated);
        registerCreated1.setInventory(inventorySaved);
        registerCreated2.setInventory(inventorySaved);
        registerCreated3.setInventory(inventorySaved);
        registerCreated4.setInventory(inventorySaved);
        inventorySaved.addRegister(registerCreated1);
        inventorySaved.addRegister(registerCreated2);
        inventorySaved.addRegister(registerCreated3);
        inventorySaved.addRegister(registerCreated4);
        Register registerSaved1 = this.registerRepository.save(registerCreated1);
        Register registerSaved2 = this.registerRepository.save(registerCreated2);
        Register registerSaved3 = this.registerRepository.save(registerCreated3);
        Register registerSaved4 = this.registerRepository.save(registerCreated4);
        this.inventoryRepository.save(inventorySaved);

        List<Register> registersFound = this.registerRepository.findRegistersByInventory_idAndProduct_idIn(inventorySaved.getId(),
                Arrays.asList(productSaved1.getId(), productSaved2.getId(),productSaved3.getId()));

        assertThat(registersFound).contains(registerSaved1, registerSaved2, registerSaved3)
                .doesNotContain(registerSaved4);
    }
    
    @Test
    public void whenRegistersRemovedAndFindInventory_ThenRegistersRemainRemoved() {

        Product productCreated1 = TestUtils.generateRandomProductWithoutId();
        Product productCreated2 = TestUtils.generateRandomProductWithoutId();
        Product productCreated3 = TestUtils.generateRandomProductWithoutId();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated = TestUtils.generateRandomRegister();
        Register registerCreated2 = TestUtils.generateRandomRegister();
        Register registerCreated3 = TestUtils.generateRandomRegister();
        registerCreated.setProduct(productCreated1);
        registerCreated2.setProduct(productCreated2);
        registerCreated3.setProduct(productCreated3);

        this.productTypeRepository.save(productCreated1.getProductType());
        this.productTypeRepository.save(productCreated2.getProductType());
        this.productTypeRepository.save(productCreated3.getProductType());
        this.productRepository.save(productCreated1);
        this.productRepository.save(productCreated2);
        this.productRepository.save(productCreated3);
        Device deviceSaved = this.deviceRepository.save(deviceCreated);
        inventoryCreated.setDevice(deviceSaved);
        Inventory inventorySaved = this.inventoryRepository.save(inventoryCreated);
        registerCreated.setInventory(inventorySaved);
        registerCreated2.setInventory(inventorySaved);
        registerCreated3.setInventory(inventorySaved);
        inventorySaved.addRegister(registerCreated);
        inventorySaved.addRegister(registerCreated2);
        inventorySaved.addRegister(registerCreated3);
        Register registerSaved1 = this.registerRepository.save(registerCreated);
        Register registerSaved2 = this.registerRepository.save(registerCreated2);
        Register registerSaved3 = this.registerRepository.save(registerCreated3);
        this.inventoryRepository.save(inventorySaved);
        Inventory inventoryFound = this.inventoryRepository.findByDeviceUUID(deviceSaved.getUUID());

        assertThat(inventoryFound.getRegisters()).contains(registerSaved1, registerSaved2, registerSaved3);

        inventorySaved.getRegisters().remove(registerCreated2);
        this.registerRepository.delete(registerCreated2);
        this.inventoryRepository.save(inventorySaved);
        Inventory inventoryWithDeletedRegistersFound = this.inventoryRepository.findByDeviceUUID(deviceSaved.getUUID());

        assertThat(inventoryWithDeletedRegistersFound.getRegisters())
                .contains(registerSaved1, registerSaved3).doesNotContain(registerCreated2);
    }


}
