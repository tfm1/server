package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.ProductType;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
class ProductTypeRepositoryTest {

    @Autowired
    ProductTypeRepository productTypeRepository;

    @Test
    public void whenSavedAndFindById_thenReturnProductType() {

        ProductType t = TestUtils.generateRandomProductType();

        this.productTypeRepository.save(t);

        Optional<ProductType> productTypeFound = this.productTypeRepository.findById(t.getId());

        assertThat(productTypeFound.isPresent()).isTrue();
        ProductType found = productTypeFound.orElse(new ProductType());
        assertThat(found.getId()).isEqualTo(t.getId());
        assertThat(found.getName()).isEqualTo(t.getName());
    }

}