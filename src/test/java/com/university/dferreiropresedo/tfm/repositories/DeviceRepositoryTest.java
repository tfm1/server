package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
class DeviceRepositoryTest {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenSaveAndFindById_ThenEntityReturned() {
        Device createdDevice = TestUtils.generateRandomDevice();

        this.deviceRepository.save(createdDevice);
        Optional<Device> deviceFoundById = this.deviceRepository.findById(createdDevice.getUUID());

        assertThat(deviceFoundById.isPresent()).isTrue();

        Device deviceFound = deviceFoundById.orElse(new Device());

        assertThat(deviceFound.getUUID()).isEqualTo(createdDevice.getUUID());
        assertThat(deviceFound.getName()).isEqualTo(createdDevice.getName());
        assertThat(deviceFound.getUsers()).isEmpty();
        assertThat(deviceFound.getPossibleBuyers()).isEmpty();
        assertThat(deviceFound.getBuyer()).isNull();
    }

    @Test
    public void whenSaveWithUserAndFind_ThenUserIsAssociated() {

        Device createdDevice = TestUtils.generateRandomDevice();
        User createdUser = TestUtils.generateRandomUser();

        this.deviceRepository.save(createdDevice);
        this.userRepository.save(createdUser);

        createdDevice.establishBuyer(createdUser);
        createdDevice.addPossibleBuyer(createdUser);
        createdDevice.addUser(createdUser);

        this.deviceRepository.save(createdDevice);

        Optional<Device> deviceFound = this.deviceRepository.findById(createdDevice.getUUID());
        User userFound = this.userRepository.findByInternalId(createdUser.getInternalId());

        assertThat(deviceFound.isPresent()).isTrue();
        assertThat(userFound).isNotNull();

        Device testingDevice = deviceFound.orElse(new Device());

        assertThat(userFound.getInternalId()).isEqualTo(userFound.getInternalId());
        assertThat(testingDevice.getUUID()).isEqualTo(createdDevice.getUUID());

        assertThat(userFound.getUsingDevices()).contains(testingDevice);
        assertThat(userFound.getMayBuyFor()).contains(testingDevice);
        assertThat(userFound.getBuysFor()).contains(testingDevice);

        assertThat(testingDevice.getPossibleBuyers()).contains(userFound);
        assertThat(testingDevice.getUsers()).contains(userFound);
        assertThat(testingDevice.getBuyer()).isEqualTo(userFound);
    }

    @Test
    public void whenDeviceUserRelationshipRemoved_ThenUserIsDisassociated() {
        Device createdDevice = TestUtils.generateRandomDevice();
        User createdUser = TestUtils.generateRandomUser();
        this.deviceRepository.save(createdDevice);
        this.userRepository.save(createdUser);
        createdDevice.establishBuyer(createdUser);
        createdDevice.addPossibleBuyer(createdUser);
        createdDevice.addUser(createdUser);
        this.deviceRepository.save(createdDevice);
        this.userRepository.save(createdUser);

        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(createdDevice.getUUID());
        assertThat(optionalDeviceFound.isPresent()).isTrue();
        Device deviceFound = optionalDeviceFound.orElseGet(TestUtils::generateRandomDevice);
        deviceFound.removeUser(createdUser);
        this.deviceRepository.saveAll(Collections.singletonList(deviceFound));
        Optional<Device> optionaAnotherDeviceFound = this.deviceRepository.findById(createdDevice.getUUID());
        Device anotherDeviceFound = optionaAnotherDeviceFound.orElseGet(TestUtils::generateRandomDevice);

        assertThat(optionaAnotherDeviceFound.isPresent()).isTrue();
        assertThat(anotherDeviceFound.getUsers()).doesNotContain(createdUser);
        assertThat(deviceFound.getUsers()).doesNotContain(createdUser);
    }

    @Test
    public void whenDeviceWithAssociatedAndFindByUserAndUUID_ThenDeviceReturned() {
        Device createdDevice = TestUtils.generateRandomDevice();
        User createdUser = TestUtils.generateRandomUser();
        this.deviceRepository.save(createdDevice);
        this.userRepository.save(createdUser);
        createdDevice.establishBuyer(createdUser);
        createdDevice.addPossibleBuyer(createdUser);
        createdDevice.addUser(createdUser);
        this.deviceRepository.save(createdDevice);
        this.userRepository.save(createdUser);

        List<Device> devicesFound = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                createdUser.getInternalId(), Collections.singletonList(createdDevice.getUUID()));
        List<Device> mustBeEmpty = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                TestUtils.generateRandomUser().getInternalId(), Collections.singletonList(createdDevice.getUUID()));

        Assertions.assertThat(devicesFound).contains(createdDevice);
        Assertions.assertThat(mustBeEmpty).isEmpty();
    }

    @Test
    public void whenFindByDevicesByUsersInternalIdAndUUID_ThenCorrectDevicesReturned() {
        User createdUser1 = TestUtils.generateRandomUser();
        User createdUser2 = TestUtils.generateRandomUser();
        User createdUser3 = TestUtils.generateRandomUser();
        Device createdDevice = TestUtils.generateRandomDevice();
        Device createdDevice2 = TestUtils.generateRandomDevice();
        Device createdDevice3 = TestUtils.generateRandomDevice();
        createdDevice.addUser(createdUser1);
        createdDevice2.addUser(createdUser1);
        createdDevice2.addUser(createdUser2);
        createdDevice3.addUser(createdUser2);

        this.userRepository.save(createdUser1);
        this.userRepository.save(createdUser2);
        this.userRepository.save(createdUser3);
        this.deviceRepository.save(createdDevice);
        this.deviceRepository.save(createdDevice2);
        this.deviceRepository.save(createdDevice3);

        List<String> allDevices = Arrays.asList(createdDevice.getUUID(), createdDevice2.getUUID(), createdDevice3.getUUID());

        List<Device> user1DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                createdUser1.getInternalId(), allDevices);
        List<Device> user2DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                createdUser2.getInternalId(), allDevices);
        List<Device> user3DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                createdUser3.getInternalId(), allDevices);
        List<Device> user2device3Found = this.deviceRepository.findDevicesByUsers_internalIdLikeAndUUIDIn(
                createdUser2.getInternalId(), Collections.singletonList(createdDevice3.getUUID()));

        assertThat(user1DevicesFound).contains(createdDevice, createdDevice2).doesNotContain(createdDevice3);
        assertThat(user2DevicesFound).contains(createdDevice2, createdDevice3).doesNotContain(createdDevice);
        assertThat(user3DevicesFound).isEmpty();
        assertThat(user2device3Found).containsOnly(createdDevice3);
    }
    
    @Test
    public void whenFindByDevicesByUsersInternalId_ThenCorrectDevicesReturned() {
        User createdUser1 = TestUtils.generateRandomUser();
        User createdUser2 = TestUtils.generateRandomUser();
        User createdUser3 = TestUtils.generateRandomUser();
        Device createdDevice = TestUtils.generateRandomDevice();
        Device createdDevice2 = TestUtils.generateRandomDevice();
        createdDevice.addUser(createdUser1);
        createdDevice2.addUser(createdUser1);
        createdDevice2.addUser(createdUser2);

        this.userRepository.save(createdUser1);
        this.userRepository.save(createdUser2);
        this.userRepository.save(createdUser3);
        this.deviceRepository.save(createdDevice);
        this.deviceRepository.save(createdDevice2);

        List<Device> user1DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLike(createdUser1.getInternalId());
        List<Device> user2DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLike(createdUser2.getInternalId());
        List<Device> user3DevicesFound = this.deviceRepository.findDevicesByUsers_internalIdLike(createdUser3.getInternalId());

        assertThat(user1DevicesFound).contains(createdDevice, createdDevice2);
        assertThat(user2DevicesFound).contains(createdDevice2).doesNotContain(createdDevice);
        assertThat(user3DevicesFound).isEmpty();
    }
}