package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.User;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Test
    public void whenSavedAndFindByInternalId_ThenUserRetrieved() {

        User createdUser = TestUtils.generateRandomUser();

        this.userRepository.save(createdUser);
        User userFoundById = this.userRepository.findByInternalId(createdUser.getInternalId());

        assertThat(userFoundById).isNotNull();

        assertThat(userFoundById.getInternalId()).isEqualTo(createdUser.getInternalId());
        assertThat(userFoundById.getName()).isEqualTo(createdUser.getName());
        assertThat(userFoundById.getEmail()).isEqualTo(createdUser.getEmail());
        assertThat(userFoundById.getBuysFor()).isEmpty();
        assertThat(userFoundById.getMayBuyFor()).isEmpty();
        assertThat(userFoundById.getUsingDevices()).isEmpty();
    }

    @Test
    public void whenUserWithDevicesSavedAndFindByInternalId_ThenUserRetrievedWithDevices() {

        User createdUser = TestUtils.generateRandomUser();
        Device createdDevice = TestUtils.generateRandomDevice();
        createdDevice.addUser(createdUser);

        this.userRepository.save(createdUser);
        this.deviceRepository.save(createdDevice);

        User userFoundById = this.userRepository.findByInternalId(createdUser.getInternalId());

        assertThat(userFoundById).isNotNull();

        assertThat(userFoundById.getInternalId()).isEqualTo(createdUser.getInternalId());
        assertThat(userFoundById.getName()).isEqualTo(createdUser.getName());
        assertThat(userFoundById.getEmail()).isEqualTo(createdUser.getEmail());
        assertThat(userFoundById.getBuysFor()).isEmpty();
        assertThat(userFoundById.getMayBuyFor()).isEmpty();
        assertThat(userFoundById.getUsingDevices()).contains(createdDevice);
    }

    @Test
    public void whenSavedAndFindByEmail_ThenUserRetrieved() {
        User createdUser = TestUtils.generateRandomUser();

        this.userRepository.save(createdUser);
        User userFoundById = this.userRepository.findByEmail(createdUser.getEmail());

        assertThat(userFoundById).isNotNull();

        assertThat(userFoundById.getInternalId()).isEqualTo(createdUser.getInternalId());
        assertThat(userFoundById.getName()).isEqualTo(createdUser.getName());
        assertThat(userFoundById.getEmail()).isEqualTo(createdUser.getEmail());
        assertThat(userFoundById.getBuysFor()).isEmpty();
        assertThat(userFoundById.getMayBuyFor()).isEmpty();
        assertThat(userFoundById.getUsingDevices()).isEmpty();
    }


    @Test
    public void whenSavedMultipleAndFindByEmailIn_ThenUsersRetrieved() {

        User createdUser1 = TestUtils.generateRandomUser();
        User createdUser2 = TestUtils.generateRandomUser();
        User createdUser3 = TestUtils.generateRandomUser();
        User createdUser4 = TestUtils.generateRandomUser();

        this.userRepository.save(createdUser1);
        this.userRepository.save(createdUser2);
        this.userRepository.save(createdUser3);
        this.userRepository.save(createdUser4);

        List<User> usersFoundByEmail = this.userRepository.findByEmailIn(Arrays.asList(createdUser1.getEmail(), createdUser3.getEmail(), createdUser4.getEmail()));

        assertThat(usersFoundByEmail).contains(createdUser1, createdUser3, createdUser4).doesNotContain(createdUser2);
    }

    @Test
    public void whenSavedMultipleAndFindByInternalIdNotIn_ThenUsersRetrieved() {

        User createdUser1 = TestUtils.generateRandomUser();
        User createdUser2 = TestUtils.generateRandomUser();
        User createdUser3 = TestUtils.generateRandomUser();
        User createdUser4 = TestUtils.generateRandomUser();

        this.userRepository.save(createdUser1);
        this.userRepository.save(createdUser2);
        this.userRepository.save(createdUser3);
        this.userRepository.save(createdUser4);

        List<User> usersFound = this.userRepository.findByInternalIdNotIn(Arrays.asList(createdUser1.getInternalId(), createdUser2.getInternalId()));

        assertThat(usersFound).contains(createdUser3, createdUser4).doesNotContain(createdUser1, createdUser2);
    }
}