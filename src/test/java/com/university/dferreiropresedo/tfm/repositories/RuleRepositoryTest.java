package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
public class RuleRepositoryTest {

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;


    @Test
    public void whenSavedAndFindById_ThenReturned() {

        Product productCreated = TestUtils.generateRandomProduct();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();

        Rule ruleCreated = TestUtils.generateRandomRule();
        ruleCreated.setInventory(inventoryCreated);
        ruleCreated.setProduct(productCreated);

        inventoryCreated.getRules().add(ruleCreated);

        this.productTypeRepository.save(productCreated.getProductType());
        this.productRepository.save(productCreated);
        this.deviceRepository.save(inventoryCreated.getDevice());
        this.inventoryRepository.save(inventoryCreated);
        this.ruleRepository.save(ruleCreated);

        Optional<Rule> ruleFound = this.ruleRepository.findById(ruleCreated.getId());

        assertTrue(ruleFound.isPresent());

        Rule ruleTest = ruleFound.orElse(new Rule());

        assertThat(ruleTest.getProduct()).isEqualToComparingFieldByField(productCreated);
        assertThat(ruleTest.getInventory()).isEqualToComparingFieldByField(inventoryCreated);
        assertThat(inventoryCreated.getRules()).contains(ruleTest);

    }

    @Test
    public void whenRuleAddedToInventoryAndInventoryRetrieved_ThenInventoryWithRule() {

        Rule ruleCreated = TestUtils.generateRandomRule();
        Rule anotherRuleCreated = TestUtils.generateRandomRule();
        Inventory inventoryCreated = ruleCreated.getInventory();
        Product productCreated = ruleCreated.getProduct();
        this.productTypeRepository.save(productCreated.getProductType());
        this.productTypeRepository.save(anotherRuleCreated.getProduct().getProductType());
        this.productRepository.save(productCreated);
        this.productRepository.save(anotherRuleCreated.getProduct());
        this.deviceRepository.save(inventoryCreated.getDevice());
        this.inventoryRepository.save(inventoryCreated);
        this.ruleRepository.save(ruleCreated);
        Optional<Rule> optRuleFound = this.ruleRepository.findById(ruleCreated.getId());
        assertTrue(optRuleFound.isPresent());
        Rule ruleFound = optRuleFound.get();

        anotherRuleCreated.setInventory(ruleFound.getInventory());
        ruleCreated.getInventory().addRule(anotherRuleCreated);
        this.ruleRepository.save(anotherRuleCreated);
        this.inventoryRepository.save(ruleFound.getInventory());
        Optional<Rule> optAnotherRuleFound = this.ruleRepository.findById(anotherRuleCreated.getId());
        Optional<Inventory> optInventoryFound = this.inventoryRepository.findById(ruleCreated.getInventory().getId());

        assertTrue(optAnotherRuleFound.isPresent());
        assertTrue(optInventoryFound.isPresent());

        Rule anotherRuleFound = optAnotherRuleFound.get();
        Inventory inventoryFound = optInventoryFound.get();

        assertThat(anotherRuleFound.getInventory().getId()).isEqualTo(inventoryFound.getId());
        assertThat(inventoryFound.getRules()).contains(anotherRuleFound);
    }

    @Test
    public void whenRulesRemoved_ThenRulesRemainRemoved() {

        Rule rule1Created = TestUtils.generateRandomRule();
        Rule rule2Created = TestUtils.generateRandomRule();
        Rule rule3Created = TestUtils.generateRandomRule();
        Rule rule4Created = TestUtils.generateRandomRule();
        Inventory inventoryCreated = rule1Created.getInventory();
        rule2Created.setInventory(inventoryCreated);
        rule3Created.setInventory(inventoryCreated);
        rule4Created.setInventory(inventoryCreated);
        inventoryCreated.addRule(rule1Created);
        inventoryCreated.addRule(rule2Created);
        inventoryCreated.addRule(rule3Created);
        inventoryCreated.addRule(rule4Created);
        this.productTypeRepository.save(rule1Created.getProduct().getProductType());
        this.productTypeRepository.save(rule2Created.getProduct().getProductType());
        this.productTypeRepository.save(rule3Created.getProduct().getProductType());
        this.productTypeRepository.save(rule4Created.getProduct().getProductType());
        this.productRepository.save(rule1Created.getProduct());
        this.productRepository.save(rule2Created.getProduct());
        this.productRepository.save(rule3Created.getProduct());
        this.productRepository.save(rule4Created.getProduct());
        this.deviceRepository.save(inventoryCreated.getDevice());
        this.inventoryRepository.save(inventoryCreated);
        this.ruleRepository.save(rule1Created);
        this.ruleRepository.save(rule2Created);
        this.ruleRepository.save(rule3Created);
        this.ruleRepository.save(rule4Created);


        rule1Created.removeRuleFromInventory();
        rule3Created.removeRuleFromInventory();
        rule4Created.removeRuleFromInventory();
        this.ruleRepository.delete(rule1Created);
        this.ruleRepository.delete(rule3Created);
        this.ruleRepository.delete(rule4Created);
        this.inventoryRepository.save(rule1Created.getInventory());

        Optional<Rule> optRemainRuleFound = this.ruleRepository.findById(rule2Created.getId());
        Optional<Rule> optRuleRemoved = this.ruleRepository.findById(rule1Created.getId());

        assertThat(optRemainRuleFound).isPresent();
        assertThat(optRuleRemoved).isNotPresent();

        Optional<Inventory> optInventoryFound = this.inventoryRepository.findById(inventoryCreated.getId());
        assertThat(optInventoryFound).isPresent();

        Inventory inventoryFound = optInventoryFound.get();

        assertThat(inventoryFound.getRules()).size().isEqualTo(1);
        assertThat(inventoryFound.getRules()).contains(rule2Created);
    }

}
