package com.university.dferreiropresedo.tfm.repositories;

import com.university.dferreiropresedo.tfm.TfmApplication;
import com.university.dferreiropresedo.tfm.config.H2TestingDatabaseConfig;
import com.university.dferreiropresedo.tfm.entities.Device;
import com.university.dferreiropresedo.tfm.entities.Product;
import com.university.dferreiropresedo.tfm.entities.weak.inventory.Inventory;
import com.university.dferreiropresedo.tfm.entities.weak.register.Register;
import com.university.dferreiropresedo.tfm.entities.weak.rule.Rule;
import com.university.dferreiropresedo.tfm.util.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {
        TfmApplication.class, H2TestingDatabaseConfig.class
})
@ActiveProfiles("test")
@Transactional
@Sql(scripts = {
        "classpath:/database/populate.sql"
})
public class InventoryRepositoryTest {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private RegisterRepository registerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductTypeRepository productTypeRepository;

    @Test
    public void whenSavedAndFindById_ThenEntityReturned() {

        Device deviceCreated = TestUtils.generateRandomDevice();
        this.deviceRepository.save(deviceCreated);

        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        inventoryCreated.setDevice(deviceCreated);
        this.inventoryRepository.save(inventoryCreated);

        Optional<Device> deviceFound = this.deviceRepository.findById(deviceCreated.getUUID());
        Optional<Inventory> inventoryFound = this.inventoryRepository.findById(inventoryCreated.getId());

        assertTrue(deviceFound.isPresent());
        assertTrue(inventoryFound.isPresent());

        Device deviceTest = deviceFound.orElse(new Device());
        Inventory inventoryTest = inventoryFound.orElse(new Inventory());

        assertEquals(deviceCreated.getUUID(), deviceTest.getUUID());
        assertEquals(inventoryCreated.getId(), inventoryTest.getId());
        assertEquals(inventoryCreated.getDevice(), deviceTest);

    }

    @Test
    public void whenSavedAndFindByDevice_ThenInventoryReturned() {

        Device deviceCreated = TestUtils.generateRandomDevice();
        this.deviceRepository.save(deviceCreated);
        Optional<Device> optionalDeviceFound = this.deviceRepository.findById(deviceCreated.getUUID());
        Device deviceFound = optionalDeviceFound.orElseGet(TestUtils::generateRandomDevice);
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        inventoryCreated.setDevice(deviceFound);
        this.inventoryRepository.save(inventoryCreated);

        Inventory inventoryFound = this.inventoryRepository.findByDeviceUUID(deviceFound.getUUID());

        Assertions.assertThat(optionalDeviceFound.isPresent()).isTrue();
        Assertions.assertThat(inventoryFound).isNotNull();
        Assertions.assertThat(deviceFound.getUUID()).isEqualTo(deviceCreated.getUUID());
        Assertions.assertThat(deviceFound).isEqualTo(inventoryFound.getDevice());
    }

    @Test
    public void whenFindByDeviceUUID_ThenCorrectInventoriesAreReturned() {
        Device deviceCreated = TestUtils.generateRandomDevice();
        Device deviceCreated2 = TestUtils.generateRandomDevice();
        Device deviceCreated3 = TestUtils.generateRandomDevice();
        Inventory inventoryCreated1 = TestUtils.generateRandomInventory();
        Inventory inventoryCreated2 = TestUtils.generateRandomInventory();
        Inventory inventoryCreated3 = TestUtils.generateRandomInventory();

        Device deviceSaved1 = this.deviceRepository.save(deviceCreated);
        Device deviceSaved2 = this.deviceRepository.save(deviceCreated2);
        Device deviceSaved3 = this.deviceRepository.save(deviceCreated3);
        inventoryCreated1.setDevice(deviceSaved1);
        inventoryCreated2.setDevice(deviceSaved2);
        inventoryCreated3.setDevice(deviceSaved3);
        Inventory inventory1Saved = this.inventoryRepository.save(inventoryCreated1);
        Inventory inventory2Saved = this.inventoryRepository.save(inventoryCreated2);
        Inventory inventory3Saved = this.inventoryRepository.save(inventoryCreated3);

        List<Inventory> inventoriesFound = this.inventoryRepository.findAllByDeviceUUIDIn(
                Arrays.asList(deviceSaved2.getUUID(), deviceSaved3.getUUID()));

        Assertions.assertThat(inventoriesFound).contains(inventory2Saved, inventory3Saved)
                .doesNotContain(inventory1Saved);
    }

    @Test
    public void whenSavedWithRegistersAndFind_ThenRegistersAreReturned() {

        Product productCreated1 = TestUtils.generateRandomProductWithoutId();
        Product productCreated2 = TestUtils.generateRandomProductWithoutId();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Register registerCreated1 = TestUtils.generateRandomRegister();
        Register registerCreated2 = TestUtils.generateRandomRegister();
        registerCreated1.setProduct(productCreated1);
        registerCreated2.setProduct(productCreated2);

        this.productTypeRepository.save(productCreated1.getProductType());
        this.productTypeRepository.save(productCreated2.getProductType());
        this.productRepository.save(productCreated1);
        this.productRepository.save(productCreated2);
        Device deviceSaved = this.deviceRepository.save(deviceCreated);
        inventoryCreated.setDevice(deviceSaved);
        Inventory inventorySaved = this.inventoryRepository.save(inventoryCreated);
        inventorySaved.addRegister(registerCreated1);
        inventorySaved.addRegister(registerCreated2);
        registerCreated1.setInventory(inventorySaved);
        registerCreated2.setInventory(inventorySaved);
        Register registerSaved1 = this.registerRepository.save(registerCreated1);
        Register registerSaved2 = this.registerRepository.save(registerCreated2);
        this.inventoryRepository.save(inventorySaved);

        Inventory inventoryFound = this.inventoryRepository.findByDeviceUUID(deviceSaved.getUUID());

        Assertions.assertThat(inventoryFound.getRegisters()).containsOnly(registerSaved1, registerSaved2);
    }

    @Test
    public void whenSavedWithRulesAndFind_ThenRulesAreReturned() {


        Product productCreated1 = TestUtils.generateRandomProductWithoutId();
        Product productCreated2 = TestUtils.generateRandomProductWithoutId();
        Device deviceCreated = TestUtils.generateRandomDevice();
        Inventory inventoryCreated = TestUtils.generateRandomInventory();
        Rule ruleCreated1 = TestUtils.generateRandomRule();
        Rule ruleCreated2 = TestUtils.generateRandomRule();
        ruleCreated1.setProduct(productCreated1);
        ruleCreated2.setProduct(productCreated2);


        this.productTypeRepository.save(productCreated1.getProductType());
        this.productTypeRepository.save(productCreated2.getProductType());
        this.productRepository.save(productCreated1);
        this.productRepository.save(productCreated2);
        Device deviceSaved = this.deviceRepository.save(deviceCreated);
        inventoryCreated.setDevice(deviceSaved);
        Inventory inventorySaved = this.inventoryRepository.save(inventoryCreated);
        inventorySaved.addRule(ruleCreated1);
        inventorySaved.addRule(ruleCreated2);
        ruleCreated1.setInventory(inventorySaved);
        ruleCreated2.setInventory(inventorySaved);
        Rule ruleSaved1 = this.ruleRepository.save(ruleCreated1);
        Rule ruleSaved2 = this.ruleRepository.save(ruleCreated2);
        this.inventoryRepository.save(inventorySaved);

        Inventory inventoryFound = this.inventoryRepository.findByDeviceUUID(deviceSaved.getUUID());

        Assertions.assertThat(inventoryFound.getRules()).containsOnly(ruleSaved1, ruleSaved2);
    }

}
