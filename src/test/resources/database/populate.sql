
INSERT INTO product_type(product_type_id, measure, name)
    VALUES (1, 'Kg', 'Peso');
INSERT INTO product_type(product_type_id, measure, name)
    VALUES (2, 'L', 'Liquido');


INSERT INTO product(product_id, name, tag, product_type_id)
    VALUES (1, 'Harina', 'HARINA',1);
INSERT INTO product(product_id, name, tag, product_type_id)
    VALUES (2, 'Sal', 'SAL',1);
INSERT INTO product(product_id, name, tag, product_type_id)
    VALUES (3, 'Agua', 'AGUA', 2);



INSERT INTO user(user_internal_id, email, is_admin, name, image_url)
    VALUES ('1', 'usuariocreado@gmail.com', true, 'Usuario 1', null);

INSERT INTO user(user_internal_id, email, is_admin, name, image_url)
    VALUES ('2', 'usuario2@gmail.com', false, 'Usuario 2', null);

INSERT INTO user(user_internal_id, email, is_admin, name, image_url)
    VALUES ('3', 'usuario3@gmail.com', false, 'Usuario 3', null);


INSERT INTO device(uuid, name, buyer_user_id)
    VALUES (1, 'Casa', null);

INSERT INTO device(uuid, name, buyer_user_id)
    VALUES (2, 'Piso', '2');


INSERT INTO user_device_use(uuid_device, user_internal_id)
    VALUES (1, '1');
INSERT INTO user_device_use(uuid_device, user_internal_id)
    VALUES (1, '2');
INSERT INTO user_device_use(uuid_device, user_internal_id)
    VALUES (1, '3');
INSERT INTO user_device_use(uuid_device, user_internal_id)
    VALUES (2, '1');
INSERT INTO user_device_use(uuid_device, user_internal_id)
    VALUES (2, '3');


INSERT INTO possible_buyer_user_device(uuid_dispositivo, user_internal_id)
    VALUES (1, '1');
INSERT INTO possible_buyer_user_device(uuid_dispositivo, user_internal_id)
    VALUES (2, '1');
INSERT INTO possible_buyer_user_device(uuid_dispositivo, user_internal_id)
    VALUES (2, '3');


INSERT INTO inventory(inventory_id, device_uuid)
    VALUES (1, 1);
INSERT INTO inventory(inventory_id, device_uuid)
    VALUES (2, 2);


INSERT INTO rule(rule_id, amount, inventory_id, product_id)
    VALUES (1, 20.3, 1, 1);
INSERT INTO rule(rule_id, amount, inventory_id, product_id)
    VALUES (2, 15, 1, 2);
INSERT INTO rule(rule_id, amount, inventory_id, product_id)
    VALUES (3, 15, 2, 2);
INSERT INTO rule(rule_id, amount, inventory_id, product_id)
    VALUES (4, 2, 2, 3);

INSERT INTO register(register_id, amount, inventory_id, product_id)
    VALUES (1, 20.3, 1, 1);
INSERT INTO register(register_id, amount, inventory_id, product_id)
    VALUES (2, 15, 1, 2);
INSERT INTO register(register_id, amount, inventory_id, product_id)
    VALUES (3, 7.5, 1, 3);